# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements a base device for MCCS, utilising mode inheritance."""


import json
import warnings
import weakref
from typing import Any, Generic, Optional, TypeVar

import numpy as np
from ska_control_model import AdminMode, ResultCode
from ska_tango_base.base import BaseComponentManager, SKABaseDevice
from tango.server import attribute, command, device_property

from .event_serialiser import EventSerialiser
from .mode_inheritance import ModeInheritor

ComponentManagerT = TypeVar("ComponentManagerT", bound=BaseComponentManager)
DevVarLongStringArrayType = tuple[list[ResultCode], list[Optional[str]]]


class NumpyEncoder(json.JSONEncoder):
    """A JSON encoder that can handle numpy types."""

    def default(self, o: Any) -> Any:
        """
        Convert the object to a serialisable form.

        :param o: the object to serialise.

        :return: the serialised object.
        """
        if isinstance(o, (np.ndarray, np.number)):
            return o.tolist()
        if isinstance(o, complex):
            return [o.real, o.imag]
        if isinstance(o, set):
            return list(o)
        if isinstance(o, bytes):  # pragma: py3
            return o.decode()
        return super().default(o)


class MccsBaseDevice(SKABaseDevice, Generic[ComponentManagerT]):
    """An abstract base class for an MCCS Tango device."""

    # -----------------
    # Device Properties
    # -----------------
    ParentTRL = device_property(
        dtype=str, default_value=""
    )  # Parent device to inherit modes from

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """
        Initialise this device object.

        :param args: positional args to the init
        :param kwargs: keyword args to the init
        """
        # We aren't supposed to define initialisation methods for Tango
        # devices; we are only supposed to define an `init_device` method. But
        # we insist on doing so here, just so that we can define some
        # attributes, thereby stopping the linters from complaining about
        # "attribute-defined-outside-init" etc. We still need to make sure that
        # `init_device` re-initialises any values defined in here.
        super().__init__(*args, **kwargs)

        self._mode_inheritor: ModeInheritor
        self._event_serialiser: EventSerialiser
        self._parent_trl: str

    def init_device(self) -> None:
        """Initialise the device."""
        # We want to use the EventSerialiser in the ComponentManager, so it has to be
        # before the super() call.
        self._event_serialiser = EventSerialiser()
        super().init_device()

        # We don't have the logger until init_device finishes however, so we manually
        # set it here.
        self._event_serialiser.logger = self.logger

        self._parent_trl = self.ParentTRL
        callback_ref = weakref.WeakMethod(self._admin_mode_changed)
        self._mode_inheritor = ModeInheritor(
            self.ParentTRL,
            self.logger,
            # pylint: disable=unnecessary-lambda
            adminmode_callback=lambda adminMode: callback_ref()(
                adminMode
            ),  # type: ignore
            event_serialiser=self._event_serialiser,
        )

    def delete_device(self) -> None:
        """Delete the device."""
        self._mode_inheritor.cleanup()
        super().delete_device()

    def create_component_manager(
        self,
    ) -> BaseComponentManager:
        """
        Create and return a component manager for this device.

        :raises NotImplementedError: for no implementation
        """
        raise NotImplementedError(
            "MccsBaseDevice is abstract; implement "
            "'create_component_manager` method in a subclass."
        )

    @attribute(dtype=AdminMode, memorized=True, hw_memorized=True)
    def adminMode(self) -> AdminMode:
        """
        Read the Admin Mode of the device.

        Only overriding in order to override the setter.

        :return: Admin Mode of the device
        """
        return SKABaseDevice.adminMode.fget(self)  # pylint: disable=no-member

    # pylint: disable=arguments-differ
    @adminMode.write  # type: ignore[no-redef]
    def adminMode(self, value: AdminMode) -> None:
        """
        Set the Admin Mode of the device directly.

        Mode inheritance is turned off and the AdminModel is updated.
        :param value: Admin Mode of the device.
        """
        SKABaseDevice.adminMode.fset(self, value)  # pylint: disable=no-member
        self._mode_inheritor.inheriting = False

    def _admin_mode_changed(self, admin_mode: AdminMode) -> None:
        """
        Handle an Admin Mode change from the parent.

        Callback from the ModeInheritor. Keep the AdminModeModel
        in sync.
        :param admin_mode: the new adminMode value
        """
        if admin_mode is not None:
            with warnings.catch_warnings():
                warnings.filterwarnings("ignore", category=UserWarning)
                self.admin_mode_model._straight_to_state(AdminMode(admin_mode).name)
            if (
                admin_mode in (AdminMode.ONLINE, AdminMode.ENGINEERING)
                and not self.component_manager.communication_state == "ESTABLISHED"
            ):
                self.component_manager.start_communicating()
            elif (
                admin_mode == AdminMode.OFFLINE
                and not self.component_manager.communication_state == "DISABLED"
            ):
                self.component_manager.stop_communicating()

    @attribute(memorized=True, hw_memorized=True)
    def inheritModes(self) -> bool:
        """Read the mode inheritance attribute.

        :return: True if inheriting modes, False otherwise
        """
        return self._mode_inheritor.inheriting

    @inheritModes.write  # type: ignore[no-redef]
    def inheritModes(self, inherit: bool) -> None:
        """Turn on/off mode inheritance.

        :param inherit: True to inherit modes, False to stop inheriting
        """
        self._mode_inheritor.inheriting = inherit

    @attribute(dtype="DevString")
    def parentTRL(self) -> str:
        """Return the current parent TRL.

        :return: the current parent TRL.
        """
        return self._parent_trl

    def _change_parent_device(self, trl: str) -> None:
        self._parent_trl = trl
        current_inherit = self._mode_inheritor.inheriting
        self._mode_inheritor.cleanup()
        callback_ref = weakref.WeakMethod(self._admin_mode_changed)
        self._mode_inheritor = ModeInheritor(
            trl,
            self.logger,
            # pylint: disable=unnecessary-lambda
            adminmode_callback=lambda adminMode: callback_ref()(
                adminMode
            ),  # type: ignore
            event_serialiser=self._event_serialiser,
        )
        self._mode_inheritor.inheriting = current_inherit

    @command(
        dtype_in="DevString",
        dtype_out="DevVarLongStringArray",
    )
    def SetParentTRL(
        self,
        new_parent_trl: str,
    ) -> DevVarLongStringArrayType:
        """
        Set the parent TRL for this device.

        Typical use case in the observation devices, where the parent is dependent
        on allocation.

        :param new_parent_trl: the TRL of the new parent device.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        """
        self._change_parent_device(new_parent_trl)
        return [ResultCode.OK], [f"New parent TRL set to {new_parent_trl}"]

    @command(dtype_out="DevVarLongStringArray")
    def ResetParentTRL(
        self,
    ) -> DevVarLongStringArrayType:
        """
        Reset the parent TRL for this device to the one supplied initially.

        Typical use case in the observation devices, where the parent is dependent
        on allocation.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        """
        self._change_parent_device(self.ParentTRL)
        return [ResultCode.OK], [f"New parent TRL set to {self.ParentTRL}"]

    @attribute(dtype="DevString")
    def eventHistory(self) -> str:
        """
        Return the event history of the event serialiser.

        :returns: the event history of the event serialiser.
        """
        return json.dumps(
            self._event_serialiser.event_history,
            cls=NumpyEncoder,
        )
