# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements a component manager for a ska-tango-base device."""
from __future__ import annotations

import functools
import json
import logging
import threading
from dataclasses import dataclass
from typing import Any, Callable, Dict, Optional

from ska_control_model import ResultCode, TaskStatus

from .command_proxy import MccsCommandProxy

__all__ = [
    "CommandRule",
    "CommandResult",
    "CompositeCommandResultEvaluator",
    "MccsCompositeCommandProxy",
    "pretty_print",
    "pretty_format",
]


def pretty_format(message: str, indent: int = 4) -> str | None:
    """
    Format a message with specified indent.

    :param message: the message to format
    :param indent: An optional indent parameter.
        default 4

    :raises ValueError: if the message passed could not be serialiased.
    :return: a formatted message with specified indent.
    """
    try:
        return json.dumps(json.loads(message), indent=indent)
    except json.JSONDecodeError as exc:
        raise ValueError("Unable to decode message") from exc


def pretty_print(message: str, indent: int = 4) -> None:
    """
    Print a pretty message.

    :param message: the message to pretty_print
    :param indent: An optional indent parameter.
        default 4
    """
    print(pretty_format(message=message, indent=indent))


@dataclass
class CommandResult:
    """Represents the result of a Command."""

    name: str
    status: TaskStatus
    result_code: ResultCode
    result_message: str


@dataclass
class CommandRule:
    """Defines the rules for command result validation."""

    proxy_name: str
    command_name: str
    allowed_result: list[tuple[ResultCode, str]] | None


def _is_message_subset(message1: str, message2: str) -> bool:
    """
    Check if one message is a subset of the other.

    :param message1: the first message
    :param message2: the second message

    :return: True is message1 is a subset of message2 or
        message2 is a subset of message1.
    """
    set1 = set(message1.split())
    set2 = set(message2.split())
    return set1.issubset(set2) or set2.issubset(set1)


def _is_result_valid(
    result: CommandResult, command_rules: list[CommandRule] | None
) -> bool:
    """
    Check if a result is allowed given a list of commandRule.

    :param result: the result
    :param command_rules: the rules to to help evaluate.

    :return: True if result is in ResultCode.OK or result is allowed
        by the command_rules.
    """
    # If the result_code is OK, consider it valid
    if result.result_code == ResultCode.OK:
        return True

    if command_rules is None:
        return False

    # Otherwise, check against the command rules
    for rule in command_rules:
        if rule.allowed_result:
            for allowed_code, allowed_message in rule.allowed_result:
                if result.result_code == allowed_code and _is_message_subset(
                    result.result_message, allowed_message
                ):
                    return True
    return False


def check_all_results(
    results: list[CommandResult], command_rules: list[CommandRule] | None = None
) -> bool:
    """
    Check that every result is ResultCode.OK or allowed by the CommandRules.

    :param results: a list of CommandResults to check.
    :param command_rules: a list of rules to evaluate result against.

    :return: True if all CommandResults are ResultCode.OK or
        allowed by the CommandRules.
    """
    for result in results:
        if not _is_result_valid(result, command_rules):
            return False
    return True


def _package_result(result: CommandResult) -> dict[str, Any]:
    """
    Package an CommandResult for reporting.

    :param result: The result to package.

    :return: A dictionary containing the result details.
    """
    try:
        info = json.loads(result.result_message)
    except json.JSONDecodeError:
        info = result.result_message
    return {
        "action": result.name,
        "result_code": ResultCode(result.result_code).name,
        "result_info": info,
    }


def _filter_non_ok_results(data: Dict[str, Any]) -> Optional[Dict[str, Any]]:
    """
    Filter a dictionary to only include non-OK results.

    :param data: Input dictionary containing actions and result codes.

    :return: A filtered dictionary with only non-"OK" result_codes or None if no
        such paths exist
    """
    if isinstance(data, dict):
        # Check if the dictionary contains a result_code and if it's not "OK"
        if "result_code" in data and data["result_code"] != "OK":
            return data  # Return the dictionary as is if result_code is not OK

        # Recursively explore the nested dictionaries
        new_data: Dict[str, Any] = {}
        for key, value in data.items():
            filtered_value = _filter_non_ok_results(value)
            if filtered_value:  # If filtered_value is not empty, include it
                new_data[key] = filtered_value

        # Return the new_data if it's not empty, otherwise return None
        return new_data if new_data else None

    return None  # Return None if it's not a dictionary


# pylint: disable=too-few-public-methods
class CompositeCommandResultEvaluator:
    """
    Evaluate if the results are allowed.

    A list of CommandRules can be given to this class.
    These will be used to determine if the CompositeCommandProxy
    executed successfully or not.

    NOTE: if no command_rules are given, and subcommand that returns
    a result code that is NOT ResultCode.OK will be unexpected
    and cause the evaluation of the composite to be ResultCode.FAILED.
    """

    def __init__(
        self: CompositeCommandResultEvaluator,
        command_rules: list[CommandRule] | None = None,
    ) -> None:
        """
        Construct a new CompositeCommandResultEvaluator.

        :param command_rules: An optional list of CommandRules
            to evaluate results.
        """
        self._command_rules = command_rules

    def evaluate(
        self: CompositeCommandResultEvaluator, results: list[CommandResult]
    ) -> ResultCode:
        """
        Evaluate the results to determine is composite was a success.

        This will evaluate results againt a list of rules.

        :param results: the results of the sub-commands.

        :return: the evaluated result.
        """
        return (
            ResultCode.OK
            if check_all_results(results, self._command_rules)
            else ResultCode.FAILED
        )


class MccsCompositeCommandProxy:
    """
    Manages the concurrent execution of multiple MccsCommandProxy.

    This class will concurrently execute the `MccsCommandProxy` subfunctions
    and monitor their status. Once all subfunctions are complete or the timeout
    period is complete, the task_callback will be called.
    """

    def __init__(self, logger: logging.Logger) -> None:
        """
        Initialise the proxy.

        :param logger: a logger for this object to use
        """
        self._evaluated_result: tuple[ResultCode, str] | None = None
        self._command_evaluator: CompositeCommandResultEvaluator
        self._task_callback: Callable | None = None
        self._subfunction: list[MccsCommandProxy] = []
        self.command_complete = threading.Event()
        self._command_results: dict[str, CommandResult | None] = {}
        self.logger: logging.Logger = logger

    def _try_task_callback(self, **kwargs: Any) -> None:
        if self._task_callback is not None:
            try:
                self._task_callback(**kwargs)
            except Exception as e:  # pylint: disable=broad-exception-caught
                self.logger.error(
                    f"Could not invoke task callback: exception {repr(e)}."
                )

    def __iadd__(
        self: MccsCompositeCommandProxy, command_proxy: MccsCommandProxy
    ) -> MccsCompositeCommandProxy:
        """
        Add a command proxy as a subfunction.

        :param command_proxy: the `MccsCommandProxy` to be added.

        :return: The current instance with the subfunction added.
        """
        self._command_results[command_proxy.name()] = None
        self._subfunction.append(command_proxy)
        return self

    def _subfunction_task_callback(
        self: MccsCompositeCommandProxy, task_name: str, **kwargs: Any
    ) -> None:
        """
        Handle the completion of a subfunction.

        :param task_name: The name of the subcommand.
        :param kwargs: Additional task parameters, including result and status.
        """
        if "result" in kwargs and "status" in kwargs:
            self._command_results[task_name] = CommandResult(
                task_name, kwargs["status"], kwargs["result"][0], kwargs["result"][1]
            )
        if all(
            isinstance(value, CommandResult) for value in self._command_results.values()
        ):
            command_results: list[CommandResult] = []
            for command_result in self._command_results.values():
                assert command_result is not None
                command_results.append(command_result)

            self._evaluated_result = self._evaluate_result(command_results)
            self._try_task_callback(
                status=TaskStatus.COMPLETED, result=self._evaluated_result
            )
            self.command_complete.set()

    def _evaluate_result(
        self: MccsCompositeCommandProxy, command_results: list[CommandResult]
    ) -> tuple[ResultCode, str]:
        """
        Evaluate the combined result of all subcommands.

        :param command_results: A list of results from the subcommands.
        :return: A tuple with the final ResultCode and a message.
        """
        result_code = self._command_evaluator.evaluate(command_results)
        _command_results: dict[str, Any] = {}
        for result in command_results:
            _command_results[result.name] = _package_result(result)
        _filtered_results = _filter_non_ok_results(_command_results)
        if _filtered_results is None:
            # There are no unhappy paths
            return (
                ResultCode(result_code),
                "MccsCompositeCommandProxy completed without warning.",
            )
        return (ResultCode(result_code), json.dumps(_filtered_results, indent=4))

    def __call__(
        self: MccsCompositeCommandProxy,
        command_evaluator: CompositeCommandResultEvaluator,
        task_callback: Callable | None = None,
        timeout: int = 40,
    ) -> tuple[ResultCode, str]:
        """
        Execute the composite command proxy.

        This will execute all `MccsCommandProxy` objects added to
        this composite concurrently.

        :param command_evaluator: An evaluator to assess the subcommand results.
        :param task_callback: an optional callable to call when all
            `MccsCommandProxy` subfunctions are complete
            or a timeout is exceeded.
        :param timeout: The maximum time to wait for a result.

        :return: A tuple with the final result code and message.
        """
        self._command_evaluator = command_evaluator
        self._task_callback = task_callback
        for function in self._subfunction:
            function(
                task_callback=functools.partial(
                    self._subfunction_task_callback, function.name()
                ),
                timeout=timeout,
            )
        self._try_task_callback(status=TaskStatus.IN_PROGRESS)
        self.command_complete.wait(timeout)
        if not self._evaluated_result:
            message = f"No result was found after {timeout} seconds"
            self.logger.warning(message)
            return (ResultCode.UNKNOWN, message)
        return self._evaluated_result
