# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains unit tests of mode inheritance."""
from __future__ import annotations

import logging
import time
import unittest.mock
from typing import Any, Callable, Iterator

import pytest
import tango
from ska_control_model import AdminMode
from ska_tango_testing.context import TangoContextProtocol
from ska_tango_testing.harness import TangoTestHarness, TangoTestHarnessContext
from ska_tango_testing.mock import MockCallable
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_low_mccs_common.mode_inheritance import CallbackManager, ModeInheritor


class TestCallbackManager:
    """This class contains unit tests of the CallbackManager."""

    @pytest.fixture()
    def foo_mode_callback(self) -> Callable[[int], None]:
        """
        Return a mock to be passed as a callback to be called when "foo_mode" changes.

        :return: mock to be called when the "foo_mode" changes
        """
        return unittest.mock.Mock()

    @pytest.fixture()
    def bah_mode_callback(self) -> Callable[[int], None]:
        """
        Return a mock to be passed as a callback to be called when "bah_mode" changes.

        :return: mock to be called when the "bah_mode" changes
        """
        return unittest.mock.Mock()

    @pytest.fixture()
    def callback_manager(
        self,
        foo_mode_callback: Callable,
        bah_mode_callback: Callable,
    ) -> CallbackManager:
        """
        Return a CallbackManager object to test.

        This object supports three modes: foo_mode bah_mode and nahMode.
        The last of these does not have a registered callback.

        :param foo_mode_callback: callback to call when the "foo_mode" changes.
        :param bah_mode_callback: callback to call when the "bah_mode" changes.

        :return: a CallbackManager object to test
        """
        return CallbackManager(
            foo_mode=foo_mode_callback,
            bah_mode=bah_mode_callback,
            nah_mode=None,
        )

    def test_unknown_mode_handling(
        self,
        callback_manager: CallbackManager,
    ) -> None:
        """
        Test error handling for unknown modes.

        :param callback_manager: the CallbackManager object under test.
        """
        error_message = (
            "Cannot access unknown mode 'nonexistent_mode': "
            r"known modes are: \['foo_mode', 'bah_mode', 'nah_mode'\]"
        )
        with pytest.raises(ValueError, match=error_message):
            callback_manager.update("nonexistent_mode", "its value")

    def test_inheritance(
        self,
        callback_manager: CallbackManager,
        foo_mode_callback: unittest.mock.Mock,
        bah_mode_callback: unittest.mock.Mock,
    ) -> None:
        """
        Test callback manager functionality.

        :param callback_manager: the CallbackManager object under test.
        :param foo_mode_callback: a callback that the callback manager under test
            will call whenever the foo_mode changes.
        :param bah_mode_callback: a callback that the callback manager under test
            will call whenever the bah_mode changes.
        """
        assert not callback_manager.active

        callback_manager.update("foo_mode", 1)
        foo_mode_callback.assert_not_called()

        callback_manager.update("bah_mode", 2)
        foo_mode_callback.assert_not_called()

        callback_manager.update("nah_mode", 3)

        callback_manager.active = True

        foo_mode_callback.assert_called_once_with(1)
        bah_mode_callback.assert_called_once_with(2)

    def test_toggle(
        self,
        callback_manager: CallbackManager,
        foo_mode_callback: unittest.mock.Mock,
    ) -> None:
        """
        Test that the mode_callback is called only when active.

        :param callback_manager: the CallbackManager object under test.
        :param foo_mode_callback: a callback that the callback manager
            under test will call whenever the foo_mode changes.
        """
        assert not callback_manager.active

        callback_manager.update("foo_mode", 1)
        foo_mode_callback.assert_not_called()

        callback_manager.update("foo_mode", 2)
        foo_mode_callback.assert_not_called()

        callback_manager.active = True
        foo_mode_callback.assert_called_once_with(2)
        foo_mode_callback.reset_mock()

        callback_manager.active = False
        callback_manager.update("foo_mode", 3)
        foo_mode_callback.assert_not_called()

        callback_manager.active = True
        foo_mode_callback.assert_called_once_with(3)
        foo_mode_callback.reset_mock()

        callback_manager.update("foo_mode", 4)
        foo_mode_callback.assert_called_once_with(4)
        foo_mode_callback.reset_mock()

        callback_manager.active = False
        callback_manager.update("foo_mode", 5)
        foo_mode_callback.assert_not_called()

        callback_manager.active = True
        foo_mode_callback.assert_called_once_with(5)
        foo_mode_callback.reset_mock()


class TestModeInheritor:
    """Tests of the mode inheritor."""

    @pytest.fixture(name="adminmode_callback")
    def fixture_adminmode_callback(self) -> MockCallable:
        """
        Return a mock to be passed as a callback to be called when adminMode changes.

        :return: mock to be called when the adminMode changes
        """
        return MockCallable()

    @pytest.fixture(name="parent_device_trl", scope="session")
    def fixture_parent_device_trl(self) -> str:
        """
        Return the TRL of the mode inheritor's parent device.

        :return: the TRL of the mode inheritor's parent device.
        """
        return "test/parent/parent"

    @pytest.fixture(name="test_context")
    def fixture_test_context(
        self, parent_device_trl: str
    ) -> Iterator[TangoTestHarnessContext]:
        """
        Yield a test context in which the mode inheritor's parend device is running.

        :param parent_device_trl: TRL of the mode inheritor's parent Tango device.

        :yields: a test context in which the mode inheritor's parend device is running.
        """

        class _AdminModeDevice(tango.server.Device):
            """A Tango device with a writable adminMode attribute."""

            def __init__(self, *args: Any, **kwargs: Any) -> None:
                self._adminmode: AdminMode
                super().__init__(*args, **kwargs)

            def init_device(self) -> None:
                self._adminmode = AdminMode.OFFLINE
                self.set_change_event("adminMode", True)

            @tango.server.attribute(dtype=AdminMode)
            def adminMode(self) -> str:  # noqa: N802
                """
                Return the admin mode of the device.

                :return: the admin mode of the device
                """
                return self._adminmode

            @adminMode.write  # type: ignore[no-redef]
            def adminMode(self, adminmode: AdminMode) -> None:  # noqa: N802
                """
                Set the admin mode of the device.

                :param adminmode: the admin mode of the device
                """
                self._adminmode = adminmode
                self.push_change_event("adminMode", adminmode)

        test_harness = TangoTestHarness()
        test_harness.add_device(parent_device_trl, _AdminModeDevice)

        with test_harness as test_harness_context:
            yield test_harness_context

    @pytest.fixture(name="parent_device")
    def fixture_parent_device(
        self, test_context: TangoTestHarnessContext, parent_device_trl: str
    ) -> tango.DeviceProxy:
        """
        Return a proxy to the mode inheritor's parent device.

        :param test_context: a test context in which the parent device is running
        :param parent_device_trl: TRL of the mode inheritor's parent Tango device.

        :return: a proxy to the mode inheritor's parent device.
        """
        return test_context.get_device(parent_device_trl)

    @pytest.fixture(name="mode_inheritor")
    def fixture_mode_inheritor(
        self,
        test_context: TangoTestHarnessContext,
        parent_device_trl: str,
        logger: logging.Logger,
        adminmode_callback: unittest.mock.Mock,
    ) -> ModeInheritor:
        """
        Return a mode inheritor for testing.

        :param test_context: a test context in which the parent device is running
        :param parent_device_trl: TRL of the mode inheritor's parent Tango device.
        :param logger: a logger for the mode inheritor to use.
        :param adminmode_callback: callable that the mode inheritor calls
            when its adminMode changes.

        :return: a mode inheritor for testing.
        """
        return ModeInheritor(
            parent_device_trl,
            logger,
            adminmode_callback=adminmode_callback,
        )

    def test_mode_inheritor(
        self,
        parent_device: tango.get_device_proxy,
        mode_inheritor: ModeInheritor,
        adminmode_callback: MockCallable,
    ) -> None:
        """
        Test core functionality of the mode inheritor.

        Test that when inheriting attribute is True, it inherits;
        and when inheriting attribute is False, it takes its local override value.

        :param parent_device: a proxy to the mode inheritor's parent Tango device.
        :param mode_inheritor: the mode inheritor object under test.
        :param adminmode_callback: callable that the mode inheritor calls
            when its adminMode changes.
        """
        adminmode_callback.assert_not_called()

        # Set in inheriting mode
        mode_inheritor.inheriting = True
        adminmode_callback.assert_call(AdminMode.OFFLINE)

        parent_device.adminMode = AdminMode.ONLINE
        adminmode_callback.assert_call(AdminMode.ONLINE)

        # Override the inheritance
        mode_inheritor.inheriting = False
        adminmode_callback.assert_not_called()

        parent_device.adminMode = AdminMode.NOT_FITTED
        adminmode_callback.assert_not_called()

        # Back to inheriting mode
        mode_inheritor.inheriting = True
        adminmode_callback.assert_call(AdminMode.NOT_FITTED)

    def test_initial_value_is_written(self, mode_inheritor: ModeInheritor) -> None:
        """
        Test that inheriting attribute is eventually set to a default if not written to.

        That is, test that if the `inheriting` attribute is not initially written to,
        indicating the absence of a memorized value for it,
        then it eventually defaults to True.

        :param mode_inheritor: the mode inheritor object under test.
        """
        assert not mode_inheritor.inheriting
        time.sleep(ModeInheritor.INITIAL_VALUE_TIMEOUT + 1)
        assert mode_inheritor.inheriting

    def test_initial_value_is_not_overwritten(
        self, mode_inheritor: ModeInheritor
    ) -> None:
        """
        Test that initial writes to the inheriting attribute are honoured.

        That is, test that if we explicitly write to the `inheriting` attribute,
        right after device launch, as a memorized attribute would do,
        then that write is honoured, and not overwritten.

        :param mode_inheritor: the mode inheritor object under test.
        """
        assert not mode_inheritor.inheriting
        mode_inheritor.inheriting = False
        time.sleep(ModeInheritor.INITIAL_VALUE_TIMEOUT + 1)
        assert not mode_inheritor.inheriting


class TestBaseDevice:
    """Test mode inheritance using the MccsBaseDevice."""

    @pytest.fixture(name="change_event_callbacks")
    def change_event_callbacks_fixture(self) -> MockTangoEventCallbackGroup:
        """
        Return a dictionary of change event callbacks with asynchrony support.

        :return: a collections.defaultdict that returns change event
            callbacks by name.
        """
        return MockTangoEventCallbackGroup(
            "adminMode",
            timeout=ModeInheritor.INITIAL_VALUE_TIMEOUT + 1,
            assert_no_error=False,
        )

    def test_inherit_mode_change(
        self,
        mccs_base_tango_test_harness: TangoContextProtocol,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """Test that we can inherit a mode change from a parent.

        :param mccs_base_tango_test_harness: the test harness containing
            parent and child devices
        :param change_event_callbacks: adminMode change event callbacks
        """
        parent_trl = "mock/mock/1"
        child_trl = "mock/mock/2"

        parent_device = mccs_base_tango_test_harness.get_device(parent_trl)
        child_device = mccs_base_tango_test_harness.get_device(child_trl)
        child_device.inheritModes = True

        child_device.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )

        parent_device.adminMode = AdminMode.ONLINE

        # Need to swallow the first change event which is an error indicating
        # that the value hasn't been set yet
        change_event_callbacks.assert_change_event(
            "adminMode", AdminMode.ONLINE, lookahead=2, consume_nonmatches=True
        )
        assert child_device.adminMode == AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_not_called()

        parent_device.adminMode = AdminMode.OFFLINE
        change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)
        assert child_device.adminMode == AdminMode.OFFLINE
        change_event_callbacks["adminMode"].assert_not_called()

        parent_device.adminMode = AdminMode.ONLINE
        change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)
        assert child_device.adminMode == AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_not_called()

    def test_override_inheriting(
        self,
        mccs_base_tango_test_harness: TangoContextProtocol,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """Test that we can override mode changes from a parent.

        :param mccs_base_tango_test_harness: the test harness containing
            parent and child devices
        :param change_event_callbacks: adminMode change event callbacks
        """
        parent_trl = "mock/mock/1"
        child_trl = "mock/mock/2"

        parent_device = mccs_base_tango_test_harness.get_device(parent_trl)
        child_device = mccs_base_tango_test_harness.get_device(child_trl)
        child_device.inheritModes = True

        child_device.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )

        parent_device.adminMode = AdminMode.ONLINE

        # Need to swallow the first change event which is an error indicating
        # that the value hasn't been set yet
        change_event_callbacks["adminMode"].assert_change_event(
            AdminMode.ONLINE, lookahead=2, consume_nonmatches=True
        )
        assert child_device.adminMode == AdminMode.ONLINE

        # Set adminMode directly on the child device
        child_device.adminMode = AdminMode.OFFLINE
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.OFFLINE)
        assert child_device.adminMode == AdminMode.OFFLINE
        change_event_callbacks["adminMode"].assert_not_called()

        # Now changes on the parent should not affect the child
        parent_device.adminMode = AdminMode.OFFLINE
        change_event_callbacks["adminMode"].assert_not_called()

        parent_device.adminMode = AdminMode.ONLINE
        change_event_callbacks["adminMode"].assert_not_called()
        assert child_device.adminMode == AdminMode.OFFLINE

        # Go back to inheriting
        child_device.inheritModes = True
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ONLINE)
        assert child_device.adminMode == AdminMode.ONLINE

        parent_device.adminMode = AdminMode.ENGINEERING
        change_event_callbacks["adminMode"].assert_change_event(AdminMode.ENGINEERING)
        assert child_device.adminMode == AdminMode.ENGINEERING

    def test_state_machine(
        self,
        mccs_base_tango_test_harness: TangoContextProtocol,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """Test state transitions work correctly.

        :param mccs_base_tango_test_harness: the test harness containing
            parent and child devices
        :param change_event_callbacks: adminMode change event callbacks
        """
        parent_trl = "mock/mock/1"
        child_trl = "mock/mock/2"

        parent_device = mccs_base_tango_test_harness.get_device(parent_trl)
        child_device = mccs_base_tango_test_harness.get_device(child_trl)
        child_device.inheritModes = True

        child_device.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )

        # Drive adminMode through inheritance
        parent_device.adminMode = AdminMode.ONLINE
        change_event_callbacks.assert_change_event(
            "adminMode", AdminMode.ONLINE, lookahead=2, consume_nonmatches=True
        )
        assert child_device.adminMode == AdminMode.ONLINE

        parent_device.adminMode = AdminMode.OFFLINE
        change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)
        assert child_device.adminMode == AdminMode.OFFLINE

        parent_device.adminMode = AdminMode.NOT_FITTED
        change_event_callbacks.assert_change_event("adminMode", AdminMode.NOT_FITTED)
        assert child_device.adminMode == AdminMode.NOT_FITTED

        # We shouldn't be able to go ONLINE now
        # Try first from the parent device
        with pytest.raises(tango.DevFailed):
            parent_device.adminMode = AdminMode.ONLINE
        assert child_device.adminMode == AdminMode.NOT_FITTED
        assert parent_device.adminMode == AdminMode.NOT_FITTED

        # Try directly from the child
        with pytest.raises(tango.DevFailed):
            child_device.adminMode = AdminMode.ONLINE
        assert child_device.adminMode == AdminMode.NOT_FITTED
        assert parent_device.adminMode == AdminMode.NOT_FITTED

        parent_device.adminMode = AdminMode.OFFLINE
        change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)
        assert child_device.adminMode == AdminMode.OFFLINE
