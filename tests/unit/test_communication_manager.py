# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements tests for the CommunicationManager class."""
from logging import Logger
from unittest.mock import Mock

import pytest
from ska_control_model import CommunicationStatus
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs_common.communication_manager import CommunicationManager
from ska_low_mccs_common.component import DeviceComponentManager


@pytest.fixture(name="mock_device_component_manager")
def mock_device_component_manager_fixture() -> Mock:
    """
    Return a mock DeviceComponentManager for testing.

    :returns: a mock DeviceComponentManager for testing.
    """
    return Mock(spec=DeviceComponentManager)


@pytest.fixture(name="communication_manager")
def communication_manager_fixture(
    callbacks: MockCallableGroup,
    mock_device_component_manager: Mock,
) -> CommunicationManager:
    """
    Return a CommunicationManager for testing.

    :param callbacks: a dictionary of callables to be used as callbacks.
    :param mock_device_component_manager: a mock DeviceComponentManager for testing.

    :returns: a CommunicationManager for testing.
    """
    return CommunicationManager(
        callbacks["communication_status"],
        callbacks["component_state"],
        Logger("TestLogger"),
        {
            "device1": mock_device_component_manager,
            "device2": mock_device_component_manager,
            "device3": mock_device_component_manager,
        },
    )


def test_initial_status(
    communication_manager: CommunicationManager,
    callbacks: MockCallableGroup,
) -> None:
    """
    Test that the initial status of the CommunicationManager is as expected.

    :param communication_manager: a CommunicationManager for testing.
    :param callbacks: a dictionary of callables to be used as callbacks.
    """
    assert (
        communication_manager._desired_communication_status
        == CommunicationStatus.DISABLED
    )
    assert (
        communication_manager._aggregate_communication_status
        == CommunicationStatus.DISABLED
    )
    callbacks["communication_status"].assert_not_called()


def test_start_communicating(
    communication_manager: CommunicationManager, mock_device_component_manager: Mock
) -> None:
    """
    Test that the CommunicationManager starts communicating as expected.

    :param communication_manager: a CommunicationManager for testing.
    :param mock_device_component_manager: a mock DeviceComponentManager for testing.
    """
    communication_manager.start_communicating()
    assert (
        communication_manager._desired_communication_status
        == CommunicationStatus.ESTABLISHED
    )
    mock_device_component_manager.start_communicating.assert_called()


def test_stop_communicating(
    communication_manager: CommunicationManager, mock_device_component_manager: Mock
) -> None:
    """
    Test that the CommunicationManager stops communicating as expected.

    :param communication_manager: a CommunicationManager for testing.
    :param mock_device_component_manager: a mock DeviceComponentManager for testing.
    """
    communication_manager._aggregate_communication_status = (
        CommunicationStatus.ESTABLISHED
    )
    communication_manager.stop_communicating()
    assert (
        communication_manager._desired_communication_status
        == CommunicationStatus.DISABLED
    )
    mock_device_component_manager.stop_communicating.assert_called()


def test_update_communication_status(
    communication_manager: CommunicationManager,
) -> None:
    """
    Test that the CommunicationManager updates the communication status as expected.

    :param communication_manager: a CommunicationManager for testing.
    """
    communication_manager.update_communication_status(
        "device1", CommunicationStatus.ESTABLISHED
    )
    assert (
        communication_manager._device_communication_status["device1"]
        == CommunicationStatus.ESTABLISHED
    )


def test_evaluate_communication_status(
    communication_manager: CommunicationManager, callbacks: MockCallableGroup
) -> None:
    """
    Test that the CommunicationManager evaluates the communication status as expected.

    :param communication_manager: a CommunicationManager for testing.
    :param callbacks: a dictionary of callables to be used as callbacks.
    """
    communication_manager.start_communicating()
    communication_manager.update_communication_status(
        "device1", CommunicationStatus.ESTABLISHED
    )
    assert (
        communication_manager._aggregate_communication_status
        == CommunicationStatus.NOT_ESTABLISHED
    )
    callbacks["communication_status"].assert_call(CommunicationStatus.NOT_ESTABLISHED)
    communication_manager.update_communication_status(
        "device2", CommunicationStatus.ESTABLISHED
    )
    assert (
        communication_manager._aggregate_communication_status
        == CommunicationStatus.NOT_ESTABLISHED
    )
    callbacks["communication_status"].assert_not_called()
    communication_manager.update_communication_status(
        "device3", CommunicationStatus.ESTABLISHED
    )
    assert (
        communication_manager._aggregate_communication_status
        == CommunicationStatus.ESTABLISHED
    )
    callbacks["communication_status"].assert_call(CommunicationStatus.ESTABLISHED)


def test_no_device_communication(callbacks: MockCallableGroup) -> None:
    """
    Test that the CommunicationManager triggers callback as expected with no devices.

    :param callbacks: a dictionary of callables to be used as callbacks.
    """
    communication_manager = CommunicationManager(
        callbacks["communication_status"],
        callbacks["component_state"],
        Logger("TestLogger"),
        {},
    )
    communication_manager.start_communicating()
    callbacks["communication_status"].assert_call(CommunicationStatus.ESTABLISHED)
    callbacks["component_state"].assert_not_called()
    communication_manager.stop_communicating()
    callbacks["communication_status"].assert_call(CommunicationStatus.DISABLED)
    callbacks["component_state"].assert_call(power=None, fault=None)


def test_replace_device_pool(
    communication_manager: CommunicationManager,
    mock_device_component_manager: Mock,
) -> None:
    """
    Test that the CommunicationManager replaces the device pool as expected.

    :param communication_manager: a CommunicationManager for testing.
    :param mock_device_component_manager: a mock DeviceComponentManager for testing.
    """
    new_device1 = Mock(spec=DeviceComponentManager)
    new_device2 = Mock(spec=DeviceComponentManager)
    communication_manager._desired_communication_status = (
        CommunicationStatus.ESTABLISHED
    )
    communication_manager.replace_device_pool(
        {"new_device1": new_device1}, {"new_device2": new_device2}
    )
    assert communication_manager._device_pool == {
        "new_device1": new_device1,
        "new_device2": new_device2,
    }
    assert communication_manager._device_communication_status == {
        "new_device1": CommunicationStatus.DISABLED,
        "new_device2": CommunicationStatus.DISABLED,
    }
    mock_device_component_manager.stop_communicating.assert_called()
    new_device1.start_communicating.assert_called()
    new_device2.start_communicating.assert_called()
