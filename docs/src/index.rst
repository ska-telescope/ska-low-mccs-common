=======================================
MCCS LMC Common Prototype documentation
=======================================

This project is developing the Local Monitoring and Control (LMC)
prototype for the `Square Kilometre Array`_.

.. _Square Kilometre Array: https://skatelescope.org/

.. toctree::
   :maxdepth: 1
   :caption: API

   api/index

.. toctree::
   :maxdepth: 1
   :caption: Developer guide

   developer_guide/command_proxy


Indices and tables
------------------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

