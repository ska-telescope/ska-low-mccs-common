# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements infrastructure for health management in the MCCS subsystem."""

from __future__ import annotations  # allow forward references in type hints

import copy
from typing import Any, Callable, FrozenSet, Mapping, Optional

from ska_control_model import HealthState, PowerState
from typing_extensions import Protocol


class HealthModel:
    """
    A simple health model.

    It supports:

    * HealthState.UNKNOWN -- when communication with the component is
      not established.

    * HealthState.FAILED -- when the component has faulted

    * HealthState.OK -- when neither of the above conditions holds.

    This health model does not support HealthState.DEGRADED. It is up to
    subclasses to implement support for DEGRADED if required.
    """

    # TODO: THIS CLASS IS DEPRECATED.
    # See below for a BaseHealthModel class,
    # which is a temporary replacement for this class,
    # which uses kwargs instead of dictionary args,
    # as preferred by the ska-tango-testing device model.

    def __init__(
        self: HealthModel,
        component_state_callback: Callable[..., None],
    ) -> None:
        """
        Initialise a new instance.

        :param component_state_callback: callback to be called whenever
            there is a change to this this health model's evaluated
            health state.
        """
        self._communicating = False
        self._faulty = False
        self._health_state = self.evaluate_health()
        self._component_state_callback = component_state_callback
        self._component_state_callback(health=self._health_state)
        if not hasattr(self, "_health_rules"):
            self._health_rules = HealthRules()

    @property
    def health_state(self: HealthModel) -> HealthState:
        """
        Return the health state.

        :return: the health state.
        """
        return self._health_state

    def update_health(self: HealthModel) -> None:
        """
        Update health state.

        This method calls the :py:meth:``evaluate_health`` method to
        figure out what the new health state should be, and then updates
        the ``health_state`` attribute, calling the callback if
        required.
        """
        health_state = self.evaluate_health()
        if self._health_state != health_state:
            self._health_state = health_state
            self._component_state_callback(health=health_state)

    def evaluate_health(self: HealthModel) -> HealthState:
        """
        Re-evaluate the health state.

        This method contains the logic for evaluating the health. It is
        this method that should be extended by subclasses in order to
        define how health is evaluated by their particular device.

        :return: the new health state.
        """
        if not self._communicating:
            return HealthState.UNKNOWN
        if self._faulty:
            return HealthState.FAILED
        return HealthState.OK

    def component_fault(self: HealthModel, faulty: bool) -> None:
        """
        Handle a component experiencing or recovering from a fault.

        This is a callback hook that is called when the component goes
        into or out of FAULT state.

        :param faulty: whether the component has faulted or not
        """
        self._faulty = faulty
        self.update_health()

    def is_communicating(self: HealthModel, communicating: bool) -> None:
        """
        Handle change in communication with the component.

        :param communicating: whether communications with the component
            is established.
        """
        self._communicating = communicating
        self.update_health()

    @property
    def health_params(self: HealthModel) -> dict[str, float]:
        """
        Get the thresholds for health rules.

        :return: the thresholds for health rules
        """
        return self._health_rules._thresholds

    @health_params.setter
    def health_params(self: HealthModel, params: dict[str, float]) -> None:
        """
        Set the thresholds for health rules.

        :param params: A dictionary of parameters with the param name as key and
            threshold as value
        """
        self._health_rules._thresholds = self._health_rules.default_thresholds | params


# pylint: disable=too-few-public-methods
class HealthChangedCallbackProtocol(Protocol):
    """
    Specification of the callback protocol.

    The callback provided to the :py:class:~`.BaseHealthModel` is a
    callable that can be called as ``callback(health_state)`` or
    ``callback(health=health_state)``, and returns nothing.
    """

    def __call__(self, health: HealthState) -> None:
        """
        Call the health changed callback.

        :param health: the new health as evaluated by the model.
        """


class BaseHealthModel:
    """
    A simple health model.

    It supports:

    * HealthState.UNKNOWN -- when communication with the component is
      not established.

    * HealthState.FAILED -- when the component has faulted

    * HealthState.OK -- when neither of the above conditions holds.

    This health model does not support HealthState.DEGRADED. It is up to
    subclasses to implement support for DEGRADED if required.
    """

    def __init__(
        self: BaseHealthModel,
        health_changed_callback: HealthChangedCallbackProtocol,
        ignore_power_state: bool = False,
        **kwargs: Any,
    ) -> None:
        """
        Initialise a new instance.

        :param health_changed_callback: callback to be called whenever
            there is a change to this this health model's evaluated
            health state.
        :param ignore_power_state: whether the health model should ignore
            the power state when computing health.
        :param kwargs: additional keyword arguments specifying initial
            values for state.
        """
        self._state: dict[str, Any] = {
            "communicating": False,
            "fault": None,
            "power": None,
            **kwargs,
        }
        self._ignore_power_state = ignore_power_state
        self._health_state, self._health_report = self.evaluate_health()
        self._health_changed_callback = health_changed_callback
        self._health_changed_callback(health=self._health_state)
        if not hasattr(self, "_health_rules"):
            self._health_rules = HealthRules()

    @property
    def health_state(self: BaseHealthModel) -> HealthState:
        """
        Return the health state.

        :return: the health state.
        """
        return self._health_state

    @property
    def health_report(self: BaseHealthModel) -> str:
        """
        Return the health report.

        This is a short string to explain why the device is in its current health state.

        :return: a health report
        """
        return self._health_report

    def update_health(self: BaseHealthModel) -> None:
        """
        Update health state.

        This method calls the :py:meth:``evaluate_health`` method to
        figure out what the new health state should be, and then updates
        the ``health_state`` attribute, calling the callback if
        required.
        """
        health_state, health_report = self.evaluate_health()
        self._health_report = health_report
        if self._health_state != health_state:
            self._health_state = health_state
            self._health_changed_callback(health_state)

    def evalute_power_health(self: BaseHealthModel) -> tuple[HealthState, str]:
        """
        Evalute the health state w.r.t power.

        If ignore_power_state is true then return OK health
        If PowerState is ON or STANDBY then return OK health
        If PowerState is OFF or NO_SUPPLY then return last known health state

        If PowerState is UNKNOWN then return UNKNOWN health state

        :return: The health state relating to power.
        """
        return_val = HealthState.UNKNOWN, "Device is in an unhandled power state."
        if self._ignore_power_state:
            return (HealthState.OK, "Health is OK.")
        match self._state["power"]:
            case PowerState.UNKNOWN:
                return_val = HealthState.UNKNOWN, "Device is in unknown power state."
            case PowerState.NO_SUPPLY:
                return_val = (
                    self._health_state,
                    "Device has no power supply, using last known health state.",
                )
            case PowerState.OFF:
                return_val = (
                    self._health_state,
                    "Device is off, using last known health state.",
                )
            case PowerState.STANDBY:
                return_val = HealthState.OK, "Health is OK."
            case PowerState.ON:
                return_val = HealthState.OK, "Health is OK."
        return return_val

    def evaluate_health(self: BaseHealthModel) -> tuple[HealthState, str]:
        """
        Re-evaluate the health state.

        This method contains the logic for evaluating the health. It is
        this method that should be extended by subclasses in order to
        define how health is evaluated by their particular device.

        :return: the new health state and health report.
        """
        if not self._state["communicating"]:
            return HealthState.UNKNOWN, "Device is not communicating."
        if self._state["fault"]:
            return HealthState.FAILED, "Device is in fault state."
        return self.evalute_power_health()

    def update_state(self: BaseHealthModel, **kwargs: Any) -> None:
        """
        Update this health model with state relevant to evaluating health.

        :param kwargs: updated state
        """
        self._state.update(**kwargs)
        self.update_health()

    @property
    def health_params(self: BaseHealthModel) -> dict[str, float]:
        """
        Get the thresholds for health rules.

        :return: the thresholds for health rules
        """
        return self._health_rules._thresholds

    @health_params.setter
    def health_params(self: BaseHealthModel, params: dict[str, float]) -> None:
        """
        Set the thresholds for health rules.

        :param params: A dictionary of parameters with the param name as key and
            threshold as value
        """
        self._health_rules._thresholds = self._health_rules.default_thresholds | params

    def _merge_dicts(
        self: BaseHealthModel, dict_a: dict[str, Any], dict_b: dict[str, Any]
    ) -> dict[str, Any]:
        """
        Merge two nested dictionaries, taking values from b when available.

        This is necessary for nested dictionaries of thresholds

        :param dict_a: the dictionary to take from if not in dictionary b
        :param dict_b: the dictionary to preferentially take from
        :return: the merged dictionary
        """
        output = copy.deepcopy(dict_a)
        for key, new_val in dict_b.items():
            if key in dict_a:
                cur_val = dict_a[key]
                if isinstance(new_val, dict) and isinstance(cur_val, dict):
                    output[key] = self._merge_dicts(cur_val, new_val)
                else:
                    output[key] = new_val
            else:
                output[key] = new_val
        return output


class HealthRules:
    """
    A class to store health rules.

    This should be implemented by each device with health roll-up logic.
    """

    DEGRADED_THRESHOLD = 0.05
    FAILED_THRESHOLD = 0.2

    def __init__(
        self: HealthRules, thresholds: Optional[dict[str, float]] = None
    ) -> None:
        """
        Create a new instance.

        :param thresholds: the conditions for particular health states
        """
        if thresholds is None:
            self._thresholds = self.default_thresholds
        else:
            self._thresholds = self.default_thresholds | thresholds

    def get_fraction_in_states(
        self: HealthRules,
        device_dict: Mapping[str, PowerState | HealthState | None],
        states: FrozenSet[PowerState | HealthState | None],
        default: float = 0,
    ) -> float:
        """
        Get the fraction of devices in a given list of states.

        :param device_dict: dictionary of devices, key fqdn and value state
        :param states: the states to check
        :param default: the default if there are zero devices in the dictionary
        :return: the fraction of the devices in the given states
        """
        if len(device_dict) == 0:
            return default
        return self.get_count_in_states(device_dict, states) / float(len(device_dict))

    def get_count_in_states(
        self: HealthRules,
        device_dict: Mapping[str, PowerState | HealthState | None],
        states: FrozenSet[PowerState | HealthState | None],
    ) -> int:
        """
        Get the number of devices in a given list of state.

        :param device_dict: dictionary of devices, key fqdn and value state
        :param states: the states to check
        :return: the number of the devices in the given states
        """
        return sum(map(lambda s: s in states, device_dict.values()))

    @property
    def default_thresholds(self: HealthRules) -> dict[str, float]:
        """
        Get the default thresholds for this device.

        This should be overriden in a derived class to provide default thresholds.

        :return: an empty dictionary
        """
        return {}

    def failed_rule(self: HealthRules) -> tuple[bool, str]:
        """
        Rule for the FAILED state.

        :raises NotImplementedError: must be implemented in derived class
        """
        raise NotImplementedError("HealthRules is abstract")

    def degraded_rule(self: HealthRules) -> tuple[bool, str]:
        """
        Rule for the DEGRADED state.

        :raises NotImplementedError: must be implemented in derived class
        """
        raise NotImplementedError("HealthRules is abstract")

    def unknown_rule(self: HealthRules) -> tuple[bool, str]:
        """
        Rule for the UNKNOWN state.

        :raises NotImplementedError: must be implemented in derived class
        """
        raise NotImplementedError("HealthRules is abstract")

    def healthy_rule(self: HealthRules) -> tuple[bool, str]:
        """
        Rule for the OK state.

        :raises NotImplementedError: must be implemented in derived class
        """
        raise NotImplementedError("HealthRules is abstract")

    @property
    def rules(self: HealthRules) -> dict[HealthState, Callable[..., tuple[bool, str]]]:
        """
        Get the transition rules for the station.

        The rules must be implemented on a device-by-device basis.

        :return: the transition rules for the station
        """
        return {
            HealthState.FAILED: self.failed_rule,
            HealthState.DEGRADED: self.degraded_rule,
            HealthState.OK: self.healthy_rule,
            HealthState.UNKNOWN: self.unknown_rule,
        }
