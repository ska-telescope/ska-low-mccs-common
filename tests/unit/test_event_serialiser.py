# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains unit tests of the EventSerialiser."""
from datetime import datetime
from logging import Logger
from unittest.mock import Mock, call

import pytest
import tango

from ska_low_mccs_common.event_serialiser import EventSerialiser


class TestEventSerialiser:
    """Unit tests for the EventSerialiser."""

    @pytest.fixture
    def event_serialiser(self) -> EventSerialiser:
        """
        Return an EventSerialiser for testing.

        :returns: an EventSerialiser for testing.
        """
        return EventSerialiser(Logger("TestLogger"))

    @pytest.fixture
    def mock_callback(self) -> Mock:
        """
        Return a mock callback for testing.

        It is configured to have a __name__ attribute for the event history.

        :returns: a mock callback for testing.
        """
        callback = Mock()
        callback.configure_mock(__name__="MyMock")
        return callback

    def test_queue_event(
        self, event_serialiser: EventSerialiser, mock_callback: Mock
    ) -> None:
        """
        Test when we queue an event, that the callback is triggered appropriately.

        :param event_serialiser: an EventSerialiser for testing.
        :param mock_callback: a mock callback to pass to the EventSerialiser.
        """
        expected_calls = []
        event_serialiser.queue_event(
            "device1", "event1", "value1", tango.AttrQuality.ATTR_VALID, mock_callback
        )
        expected_calls.append(call("event1", "value1", tango.AttrQuality.ATTR_VALID))
        event_serialiser.queue_event(
            "device2", "event2", "value2", tango.AttrQuality.ATTR_VALID, mock_callback
        )
        expected_calls.append(call("event2", "value2", tango.AttrQuality.ATTR_VALID))
        event_serialiser._event_queue.join()
        mock_callback.assert_has_calls(expected_calls, any_order=False)

    def test_event_loop_resiliency(
        self, event_serialiser: EventSerialiser, mock_callback: Mock
    ) -> None:
        """
        Test that the event loop is resilient to exceptions in the callback.

        :param event_serialiser: an EventSerialiser for testing.
        :param mock_callback: a mock callback to pass to the EventSerialiser.
        """
        mock_callback.side_effect = Exception("Test exception")
        event_serialiser.queue_event(
            "device1", "event1", "value1", tango.AttrQuality.ATTR_VALID, mock_callback
        )
        event_serialiser._event_queue.join()
        mock_callback.side_effect = None
        event_serialiser.queue_event(
            "device2", "event2", "value2", tango.AttrQuality.ATTR_VALID, mock_callback
        )
        event_serialiser._event_queue.join()
        assert mock_callback.call_count == 2
        assert mock_callback.call_args == call(
            "event2", "value2", tango.AttrQuality.ATTR_VALID
        )

    def test_event_history(
        self, event_serialiser: EventSerialiser, mock_callback: Mock
    ) -> None:
        """
        Test when we queue an event, that the history is updated appropriately.

        :param event_serialiser: an EventSerialiser for testing.
        :param mock_callback: a mock callback to pass to the EventSerialiser.
        """
        event_serialiser.queue_event(
            "device1", "event1", "value1", tango.AttrQuality.ATTR_VALID, mock_callback
        )
        event_serialiser._event_queue.join()
        assert len(event_serialiser.event_history) == 1
        event = event_serialiser.event_history[0]
        assert event[0] == "device1"
        assert event[1] == "event1"
        assert event[2] == "value1"
        assert event[3] == "ATTR_VALID"
        assert event[4] == "MyMock"
        # The event should've been executed within the last second.
        event_time = datetime.fromisoformat(event[5])
        now = datetime.now()
        diff = abs((now - event_time).total_seconds())
        assert 0 < diff < 1, f"Timestamp difference {diff} is not less than 1 second."

    def test_clear_event_history(
        self, event_serialiser: EventSerialiser, mock_callback: Mock
    ) -> None:
        """
        Test when we clear the event history, that the history is clear.

        :param event_serialiser: an EventSerialiser for testing.
        :param mock_callback: a mock callback to pass to the EventSerialiser.
        """
        event_serialiser.queue_event(
            "device1", "event1", "value1", tango.AttrQuality.ATTR_VALID, mock_callback
        )
        event_serialiser._event_queue.join()
        assert len(event_serialiser.event_history) == 1
        event_serialiser.clear_event_history()
        assert len(event_serialiser.event_history) == 0

    def test_stop(self, event_serialiser: EventSerialiser) -> None:
        """
        Test when we stop the EventSerialiser, that the worker thread stops.

        :param event_serialiser: an EventSerialiser for testing.
        """
        assert event_serialiser._worker_thread.is_alive()
        event_serialiser.stop()
        assert not event_serialiser._worker_thread.is_alive()
