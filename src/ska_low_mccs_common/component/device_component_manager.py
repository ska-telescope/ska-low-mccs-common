# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements an abstract component manager for simple object components."""
from __future__ import annotations  # allow forward references in type hints

import logging
import threading
from typing import TYPE_CHECKING, Callable, Optional

import tango
from ska_control_model import (
    AdminMode,
    CommunicationStatus,
    HealthState,
    ObsState,
    PowerState,
    TaskStatus,
)
from ska_tango_base.base import check_communicating
from ska_tango_base.executor import TaskExecutorComponentManager

from ska_low_mccs_common import MccsDeviceProxy

if TYPE_CHECKING:
    from ska_low_mccs_common import EventSerialiser

__all__ = ["DeviceComponentManager", "ObsDeviceComponentManager"]


# pylint: disable=too-many-instance-attributes
class DeviceComponentManager(TaskExecutorComponentManager):
    """An abstract component manager for a Tango device component."""

    # pylint: disable=too-many-arguments
    def __init__(
        self: DeviceComponentManager,
        name: str,
        logger: logging.Logger,
        communication_state_callback: Callable[[CommunicationStatus], None],
        component_state_callback: Callable[..., None],
        event_serialiser: Optional[EventSerialiser] = None,
    ) -> None:
        """
        Initialise a new instance.

        :param name: the name of the device
        :param logger: the logger to be used by this object.
        :param communication_state_callback: callback to be
            called when the status of the communications channel between
            the component manager and its component changes
        :param component_state_callback: callback to be
            called when the component state changes
        :param event_serialiser: an event serialiser to be used by this object
        """
        self._name: str = name
        self._proxy: Optional[MccsDeviceProxy] = None
        self._logger = logger
        self._power_state: Optional[PowerState] = None
        self._power_state_lock = threading.RLock()
        self._faulty: Optional[bool] = None
        self._health: Optional[HealthState] = None
        self._device_health_state = HealthState.UNKNOWN
        self._device_admin_mode = AdminMode.OFFLINE
        self._connection_lock = threading.Lock()
        self._event_serialiser = event_serialiser
        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=None,
            fault=None,
        )

    def get_change_event_callbacks(self: DeviceComponentManager) -> dict[str, Callable]:
        """
        Return a dictionary for each attribute and callback pair.

        :returns: a dictionary for each attribute and callback pair.
        """
        return {
            "healthState": self._device_health_state_changed,
            "adminMode": self._device_admin_mode_changed,
            "state": self._device_state_changed,
        }

    def start_communicating(self: DeviceComponentManager) -> None:
        """
        Establish communication with the component, then start monitoring.

        This is a public method that enqueues the work to be done.
        """
        self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
        _ = self.submit_task(self._connect_to_device)

    def _connect_to_device(
        self: DeviceComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Establish communication with the component, then start monitoring.

        This contains the actual communication logic that is enqueued to
        be run asynchronously.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None

        :raises ConnectionError: if the attempt to establish
            communication with the channel fails.
        """
        self._proxy = MccsDeviceProxy(
            self._name,
            self._logger,
            connect=False,
            event_serialiser=self._event_serialiser,
        )
        try:
            self._proxy.connect()
        except tango.DevFailed as dev_failed:
            self._proxy = None
            self.logger.error(f"Could not connect to '{self._name}'")
            raise ConnectionError(
                f"Could not connect to '{self._name}'"
            ) from dev_failed

        self._update_communication_state(CommunicationStatus.ESTABLISHED)
        for event, callback in self.get_change_event_callbacks().items():
            self._proxy.add_change_event_callback(event, callback)

        self._proxy.set_source(tango.DevSource.DEV)

    def stop_communicating(self: DeviceComponentManager) -> None:
        """Cease monitoring the component, and break off all communication with it."""
        _ = self.submit_task(self._disconnect_to_device)

    def _disconnect_to_device(
        self: DeviceComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Aborts all commands and disconnects from the device.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        # This is threaded as we have definite waits in the the method.
        # This was OK when we were calling adminmode changes directly, however with
        # adminmode inheritance, this method is called via a Tango change event callback
        # so any waits in that callback will cause a seg fault.
        if self._proxy is not None:
            self._proxy.unsubscribe_all_change_events()
        self._proxy = None
        self._update_communication_state(CommunicationStatus.DISABLED)
        self._update_component_state(power=None, fault=None)
        self._health = None

    @check_communicating
    def on(
        self: DeviceComponentManager, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Turn the device on.

        :param task_callback: callback to be called when the status of
            the command changes

        :return: a result code and message
        """
        return self.submit_task(self._on, task_callback=task_callback)

    def _on(
        self: DeviceComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        On command implementation that simply calls On, on its proxy.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)
        if self.power_state == PowerState.ON:
            if task_callback:
                task_callback(
                    status=TaskStatus.COMPLETED, result="PowerState already on"
                )
        else:
            assert self._proxy is not None  # for the type checker
            self._proxy.On()  # Fire and forget
            if task_callback:
                task_callback(
                    status=TaskStatus.COMPLETED, result="PowerState on completed"
                )

    @check_communicating
    def off(
        self: DeviceComponentManager, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Turn the device off.

        :param task_callback: callback to be called when the status of
            the command changes

        :return: a result code & message
        """
        return self.submit_task(self._off, task_callback=task_callback)

    def _off(
        self: DeviceComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Off command implementation that simply calls Off, on its proxy.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)
        if self.power_state == PowerState.OFF:
            if task_callback:
                task_callback(
                    status=TaskStatus.COMPLETED, result="PowerState already off"
                )
        else:
            assert self._proxy is not None  # for the type checker
            self._proxy.Off()  # Fire and forget
            if task_callback:
                task_callback(
                    status=TaskStatus.COMPLETED, result="PowerState off completed"
                )

    @check_communicating
    def standby(
        self: DeviceComponentManager, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Turn the device to standby.

        :param task_callback: callback to be called when the status of
            the command changes

        :return: a result code, or None if there was nothing to do.
        """
        if self.power_state == PowerState.STANDBY:
            return (TaskStatus.COMPLETED, "Device was already in standby mode")

        return self.submit_task(self._standby, task_callback=task_callback)

    def _standby(
        self: DeviceComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Standby command implementation that simply calls Standby, on its proxy.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)
        assert self._proxy is not None  # for the type checker
        self._proxy.Standby()  # Fire and forget
        if task_callback:
            task_callback(
                status=TaskStatus.COMPLETED, result="PowerState standby completed"
            )

    @check_communicating
    def reset(
        self: DeviceComponentManager, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Reset the device.

        :param task_callback: callback to be called when the status of
            the command changes

        :return: a result code, or None if there was nothing to do.
        """
        return self.submit_task(self._reset, task_callback=task_callback)

    def _reset(
        self: DeviceComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Reset command implementation that simply calls Reset, on its proxy.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Check for abort, defaults to None
        """
        if task_callback:
            task_callback(status=TaskStatus.IN_PROGRESS)
        assert self._proxy is not None  # for the type checker
        self._proxy.Reset()  # Fire and forget
        if task_callback:
            task_callback(status=TaskStatus.COMPLETED, result="Reset completed")

    @property
    def health(self: DeviceComponentManager) -> Optional[HealthState]:
        """
        Return the evaluated health state of the device.

        This will be either the health state that the device reports, or
        None if the device is in an admin mode that indicates that its
        health should not be rolled up.

        :return: the evaluated health state of the device.
        """
        return self._health

    @property
    def power_state(self: DeviceComponentManager) -> Optional[PowerState]:
        """
        Return the current power state of the device.

        :return: the current power state of the device.
        """
        return self._component_state["power"]

    def _device_state_changed(
        self: DeviceComponentManager,
        event_name: str,
        event_value: tango.DevState,
        event_quality: tango.AttrQuality,
    ) -> None:
        """
        Handle an change event on device state.

        :param event_name: name of the event; will always be
            "state" for this callback
        :param event_value: the new state
        :param event_quality: the quality of the change event
        """
        if event_value == tango.DevState.FAULT:
            self._update_component_state(fault=True)
            return

        power_map = {
            tango.DevState.OFF: PowerState.OFF,
            tango.DevState.STANDBY: PowerState.STANDBY,
            tango.DevState.ON: PowerState.ON,
            tango.DevState.ALARM: PowerState.ON,
        }
        power = power_map.get(event_value, PowerState.UNKNOWN)
        self._update_component_state(power=power)

    def _device_health_state_changed(
        self: DeviceComponentManager,
        event_name: str,
        event_value: HealthState,
        event_quality: tango.AttrQuality,
    ) -> None:
        """
        Handle an change event on device health state.

        :param event_name: name of the event; will always be
            "healthState" for this callback
        :param event_value: the new health state
        :param event_quality: the quality of the change event
        """
        self._device_health_state = event_value
        self._update_health()

    def _device_admin_mode_changed(
        self: DeviceComponentManager,
        event_name: str,
        event_value: AdminMode,
        event_quality: tango.AttrQuality,
    ) -> None:
        """
        Handle an change event on device admin mode.

        :param event_name: name of the event; will always be
            "adminMode" for this callback
        :param event_value: the new admin mode
        :param event_quality: the quality of the change event
        """
        self._device_admin_mode = event_value
        self._update_health()

    def _update_health(
        self: DeviceComponentManager,
    ) -> None:
        health = (
            self._device_health_state
            if self._device_admin_mode in [AdminMode.ENGINEERING, AdminMode.ONLINE]
            else None
        )
        if self._health != health:
            self._health = health
            if self._component_state_callback is not None:
                self._component_state_callback(health=self._health)


class ObsDeviceComponentManager(DeviceComponentManager):
    """An abstract component manager for a Tango observation device component."""

    # pylint: disable=too-many-arguments
    def __init__(
        self: ObsDeviceComponentManager,
        fqdn: str,
        logger: logging.Logger,
        communication_state_callback: Callable[[CommunicationStatus], None],
        component_state_callback: Callable[..., None],
        event_serialiser: Optional[EventSerialiser] = None,
    ) -> None:
        """
        Initialise a new instance.

        :param fqdn: the FQDN of the device.
        :param logger: the logger to be used by this object.
        :param communication_state_callback: callback to be
            called when the status of the communications channel between
            the component manager and its component changes.
        :param component_state_callback: callback to be called when the
            component's state changes.
        :param event_serialiser: an event serialiser to be used by this object
        """
        self._component_state_callback = component_state_callback
        self._obs_state_changed_callback = component_state_callback
        super().__init__(
            fqdn,
            logger,
            communication_state_callback,
            component_state_callback,
            event_serialiser=event_serialiser,
        )

    def get_change_event_callbacks(
        self: ObsDeviceComponentManager,
    ) -> dict[str, Callable]:
        """
        Add obsState to change event subscriptions.

        :returns: a dictionary for each attribute and callback pair.
        """
        callbacks = super().get_change_event_callbacks()
        callbacks["obsState"] = self._obs_state_changed
        return callbacks

    def _obs_state_changed(
        self: ObsDeviceComponentManager,
        event_name: str,
        event_value: ObsState,
        event_quality: tango.AttrQuality,
    ) -> None:
        """
        Handle an change event on device obs state.

        :param event_name: name of the event; will always be
            "obsState" for this callback
        :param event_value: the new admin mode
        :param event_quality: the quality of the change event
        """
        assert (
            event_name.lower() == "obsstate"
        ), f"obs state changed callback called but event_name is {event_name}."
        if self._obs_state_changed_callback is not None:
            self._obs_state_changed_callback(obsstate_changed=event_value)
