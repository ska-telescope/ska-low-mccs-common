# ska-low-mccs-mccs-common

The SKA Low MCCS Common repository provides a python package of functionality shared across MCCS repositories.


## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-mccs-common/badge/?version=latest)](https://developer.skao.int/projects/ska-low-mccs-common/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-mccs-common documentation](https://developer.skatelescope.org/projects/ska-low-mccs-common/en/latest/index.html "SKA Developer Portal: ska-low-mccs-common documentation")
