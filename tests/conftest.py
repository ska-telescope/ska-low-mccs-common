# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""
This module contains pytest fixtures other test setups.

These are common to all ska-low-mccs tests: unit, integration and
functional (BDD).
"""
from __future__ import annotations

import logging
import unittest
from typing import Any, Callable, Generator

import pytest
import tango
from ska_tango_testing.context import (
    TangoContextProtocol,
    ThreadedTestTangoContextManager,
)
from ska_tango_testing.harness import TangoTestHarness

from ska_low_mccs_common.component import MccsBaseComponentManager
from ska_low_mccs_common.mccs_base_device import MccsBaseDevice
from ska_low_mccs_common.testing.mock import MockDeviceBuilder


def pytest_sessionstart(session: pytest.Session) -> None:
    """
    Pytest hook; prints info about tango version.

    :param session: a pytest Session object
    """
    print(tango.utils.info())


@pytest.fixture(name="mock_factory")
def mock_factory_fixture() -> Callable[[], unittest.mock.Mock]:
    """
    Fixture that provides a mock factory for device proxy mocks.

    This default factory
    provides vanilla mocks, but this fixture can be overridden by test modules/classes
    to provide mocks with specified behaviours.

    :return: a factory for device proxy mocks
    """
    return MockDeviceBuilder()


@pytest.fixture(name="tango_config")
def tango_config_fixture() -> dict[str, Any]:
    """
    Fixture that returns basic configuration information for a Tango test harness.

    For example whether or not to run in a separate process.

    :return: a dictionary of configuration key-value pairs
    """
    return {"process": False}


@pytest.fixture(name="tango_harness")
def tango_harness_fixture(
    mock_factory: Callable[[], unittest.mock.Mock],
) -> Generator[TangoContextProtocol, None, None]:
    """
    Create a test harness for testing Tango devices.

    :param mock_factory: the factory to be used to build mocks

    :yields: a tango test harness
    """
    tango_context_manager = ThreadedTestTangoContextManager()
    tango_context_manager.add_mock_device("mock/mock/1", mock_factory())
    with tango_context_manager as tango_harness:
        yield tango_harness


class MccsTestBaseComponentManager(
    MccsBaseComponentManager
):  # pylint: disable=abstract-method
    """Dummy component manager for test purposes."""

    def start_communicating(self) -> None:  # type: ignore
        """Start communicating - dummy method."""

    def stop_communicating(self) -> None:  # type: ignore
        """Stop communicating - dummy method."""


class MccsTestBaseDevice(MccsBaseDevice):
    """Concrete class for test purposees."""

    def create_component_manager(self) -> MccsBaseComponentManager:
        """Create a base component manager for test purposes.

        :return: the component manager.
        """
        return MccsTestBaseComponentManager(self.logger)


@pytest.fixture(name="mccs_base_tango_test_harness")
def mccs_base_tango_test_harness_fixture() -> (
    Generator[TangoContextProtocol, None, None]
):
    """
    Create a test harness for testing MccsBase devices.

    :yields: a tango test harness with parent and child devices.
    """
    tango_test_harness = TangoTestHarness()
    tango_test_harness.add_device("mock/mock/1", MccsTestBaseDevice)
    tango_test_harness.add_device(
        "mock/mock/2", MccsTestBaseDevice, ParentTRL="mock/mock/1"
    )

    with tango_test_harness as context:
        yield context


@pytest.fixture(scope="session", name="logger")
def logger_fixture() -> logging.Logger:
    """
    Fixture that returns a default logger.

    :return: a logger
    """
    debug_logger = logging.getLogger()
    debug_logger.setLevel(logging.DEBUG)
    return debug_logger
