# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains unit tests for the ska_low_mccs_common.health module."""
from __future__ import annotations

from typing import Any, Callable, FrozenSet

import pytest
from ska_control_model import HealthState, PowerState

from ska_low_mccs_common.health import BaseHealthModel, HealthRules


class TestHealthModel:
    """This class contains unit tests for the BaseHealthModel class."""

    @pytest.fixture()
    def health_changed_callback(self: TestHealthModel) -> Callable[..., None]:
        """
        Return an empty callback to use for the health model.

        :return: callback for the health model.
        """

        def callback(*args: Any, **kwargs: Any) -> None:
            pass

        return callback

    @pytest.fixture()
    def health_model(
        self: TestHealthModel, health_changed_callback: Callable[..., None]
    ) -> BaseHealthModel:
        """
        Get a BaseHealthModel object.

        :param health_changed_callback: callback to call when the health changes.
        :return: a BaseHealthModel object.
        """
        return BaseHealthModel(health_changed_callback, False)

    def test_health_model(self: TestHealthModel, health_model: BaseHealthModel) -> None:
        """
        Tests of the base health model's calculation of health.

        :param health_model: the health model to use
        """
        # Initial healthy state
        health_model.update_state(communicating=True, fault=False, power=PowerState.ON)
        assert health_model._health_report == "Health is OK."
        assert health_model._health_state == HealthState.OK

        # Set power state to UNKNOWN - health state UNKNOWN
        health_model.update_state(power=PowerState.UNKNOWN)
        assert health_model._health_report == "Device is in unknown power state."
        assert health_model._health_state == HealthState.UNKNOWN

        # Power no supply - uses last known state, in this case UNKNOWN
        health_model.update_state(power=PowerState.NO_SUPPLY)
        health_model.update_state(fault=False)
        assert (
            health_model._health_report
            == "Device has no power supply, using last known health state."
        )
        assert health_model._health_state == HealthState.UNKNOWN

        # Reset to OK health state,
        health_model.update_state(power=PowerState.ON)
        assert health_model._health_report == "Health is OK."
        assert health_model._health_state == HealthState.OK

        # Power no supply - uses last known state, in this case OK
        health_model.update_state(power=PowerState.NO_SUPPLY)
        assert (
            health_model._health_report
            == "Device has no power supply, using last known health state."
        )
        assert health_model._health_state == HealthState.OK

        # Set fault - go to FAILED
        health_model.update_state(fault=True)
        assert health_model._health_report == "Device is in fault state."
        assert health_model._health_state == HealthState.FAILED

        # Power off - uses last known state, in this case FAILED
        health_model.update_state(power=PowerState.OFF)
        health_model.update_state(fault=False)
        assert (
            health_model._health_report
            == "Device is off, using last known health state."
        )
        assert health_model._health_state == HealthState.FAILED

        # Reset to OK health state,
        health_model.update_state(power=PowerState.ON)
        assert health_model._health_report == "Health is OK."
        assert health_model._health_state == HealthState.OK

        # Power off - uses last known state, in this case OK
        health_model.update_state(power=PowerState.OFF)
        assert (
            health_model._health_report
            == "Device is off, using last known health state."
        )
        assert health_model._health_state == HealthState.OK

        # Power standby - health ok
        health_model.update_state(power=PowerState.STANDBY)
        assert health_model._health_report == "Health is OK."
        assert health_model._health_state == HealthState.OK

        # Ignore power state - now use fault state to determine FAILED again
        health_model._ignore_power_state = True
        health_model.update_health()
        assert health_model._health_report == "Health is OK."
        assert health_model._health_state == HealthState.OK

        # Stop communicating - now UNKNOWN
        health_model.update_state(communicating=False)
        assert health_model._health_report == "Device is not communicating."
        assert health_model._health_state == HealthState.UNKNOWN

        # Start communicating and no fault, power is off but ignored - now OK again
        health_model.update_state(communicating=True, fault=False)
        assert health_model._health_report == "Health is OK."
        assert health_model._health_state == HealthState.OK

    @pytest.mark.parametrize(
        ("dict_a", "dict_b", "result_dict"),
        [
            pytest.param(
                {"A": 1, "B": 2},
                {"A": 5, "C": 3},
                {"A": 5, "B": 2, "C": 3},
                id="Non-nested dictionary merge.",
            ),
            pytest.param(
                {"A": 1, "B": {"C": 5, "D": {"E": 1, "F": {}}}},
                {"A": 5, "B": {"D": {"E": {"G": 9}, "F": {"H": 10}}, "I": 12}, "J": 8},
                {
                    "A": 5,
                    "B": {"C": 5, "D": {"E": {"G": 9}, "F": {"H": 10}}, "I": 12},
                    "J": 8,
                },
                id="Nested dict merge",
            ),
        ],
    )
    def test_merge_dicts(
        self: TestHealthModel,
        health_model: BaseHealthModel,
        dict_a: dict,
        dict_b: dict,
        result_dict: dict,
    ) -> None:
        """
        Tests of the base health model's merge dicts function.

        :param health_model: the health model to use
        :param dict_a: the first dictionary, which is not preferentially taken from
        :param dict_b: the second dictionary, which is preferentially taken from
        :param result_dict: the resulting dictionary
        """
        assert health_model._merge_dicts(dict_a, dict_b) == result_dict


class TestHealthRules:
    """This class contains unit tests for the HealthRules class."""

    @pytest.fixture()
    def health_rules(self: TestHealthRules) -> HealthRules:
        """
        Get a HealthRules object.

        :return: a HealthRules object
        """
        return HealthRules()

    @pytest.mark.parametrize(
        (
            "device_states",
            "check_states",
            "expected_count",
            "expected_frac_count",
            "default",
        ),
        [
            pytest.param(
                {"A": HealthState.OK, "B": HealthState.OK, "C": HealthState.DEGRADED},
                frozenset({HealthState.DEGRADED, HealthState.FAILED}),
                1,
                1 / float(3),
                1,
                id="One match",
            ),
            pytest.param(
                {"A": HealthState.OK, "B": HealthState.OK, "C": HealthState.UNKNOWN},
                frozenset({HealthState.DEGRADED, HealthState.FAILED}),
                0,
                0,
                1,
                id="No matches",
            ),
            pytest.param(
                {
                    "A": HealthState.OK,
                    "B": HealthState.OK,
                    "C": HealthState.DEGRADED,
                    "D": HealthState.FAILED,
                    "E": HealthState.FAILED,
                },
                frozenset({HealthState.OK, HealthState.FAILED}),
                4,
                4 / float(5),
                1,
                id="Multiple matches",
            ),
            pytest.param(
                {},
                frozenset({HealthState.OK, HealthState.FAILED}),
                0,
                0.5,
                0.5,
                id="No devices",
            ),
            pytest.param(
                {
                    "A": PowerState.ON,
                    "B": PowerState.ON,
                    "C": PowerState.OFF,
                    "D": PowerState.UNKNOWN,
                    "E": PowerState.UNKNOWN,
                },
                frozenset({PowerState.UNKNOWN}),
                2,
                2 / float(5),
                1,
                id="Multiple PowerState matches",
            ),
        ],
    )
    def test_get_count(  # pylint: disable=too-many-arguments
        self: TestHealthRules,
        health_rules: HealthRules,
        device_states: dict[str, HealthState | PowerState | None],
        check_states: FrozenSet[HealthState | PowerState | None],
        expected_count: int,
        expected_frac_count: float,
        default: float,
    ) -> None:
        """
        Tests for get_count_in_states and get_fraction_in_states.

        :param health_rules: the HealthRules object to use
        :param device_states: the input states to check
        :param check_states: the set of states to find in input_states
        :param expected_count: the expected number of matches to check_states to find
        :param expected_frac_count: the expected fraction of input_states
            to match check_states
        :param default: default fraction for no devices case
        """
        assert (
            health_rules.get_count_in_states(device_states, check_states)
            == expected_count
        )
        assert health_rules.get_fraction_in_states(
            device_states, check_states, default
        ) == pytest.approx(expected_frac_count)
