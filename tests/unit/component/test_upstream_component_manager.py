# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests of the object_component_manager module."""
from __future__ import annotations

import logging

import pytest
from ska_control_model import CommunicationStatus, PowerState, ResultCode, TaskStatus
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs_common.component import PowerSupplyProxySimulator
from ska_low_mccs_common.component.upstream_component_manager import (
    PowerSupplySimulator,
)


class TestPowerSupplyProxySimulator:
    """Tests of the PowerSupplyProxySimulator class."""

    @pytest.fixture()
    def simulator(self: TestPowerSupplyProxySimulator) -> PowerSupplySimulator:
        """
        Return a power supply simulator for use in testing.

        :return: a power supply simulator.
        """
        return PowerSupplySimulator()

    @pytest.fixture()
    def component_manager(
        self: TestPowerSupplyProxySimulator,
        logger: logging.Logger,
        callbacks: MockCallableGroup,
        simulator: PowerSupplySimulator,
    ) -> PowerSupplyProxySimulator:
        """
        Return a component manager for the component object.

        :param logger: a logger for the component manager to use
        :param callbacks: dictionary of callables with asynchronous
            assertion support, for use as callbacks in testing
        :param simulator: the power supply simulator being managed by
            this component manager.

        :return: a fake upstream power supply proxy.
        """
        power_supply_proxy_simulator = PowerSupplyProxySimulator(
            logger,
            callbacks["communication_status"],
            callbacks["component_state"],
            _simulator=simulator,
        )
        return power_supply_proxy_simulator

    def test_communication(
        self: TestPowerSupplyProxySimulator,
        simulator: PowerSupplySimulator,
        component_manager: PowerSupplyProxySimulator,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test communication from the component manager to its component.

        :param simulator: the power supply simulator being managed by
            this component manager.
        :param component_manager: a component manager for the component object.
        :param callbacks: dictionary of callables with asynchronous
            assertion support, for use as callbacks in testing
        """
        assert component_manager.communication_state == CommunicationStatus.DISABLED
        component_manager.start_communicating()
        callbacks["communication_status"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_status"].assert_call(CommunicationStatus.ESTABLISHED)
        assert component_manager.communication_state == CommunicationStatus.ESTABLISHED

        callbacks["component_state"].assert_call(
            power=PowerState.OFF,
            fault=False,
        )

        simulator.simulate_power(PowerState.UNKNOWN)
        callbacks["component_state"].assert_call(
            power=PowerState.UNKNOWN,
        )

        simulator.simulate_power(PowerState.NO_SUPPLY)
        callbacks["component_state"].assert_call(
            power=PowerState.NO_SUPPLY,
        )

        component_manager.stop_communicating()
        callbacks["communication_status"].assert_call(CommunicationStatus.DISABLED)
        assert component_manager.communication_state == CommunicationStatus.DISABLED

    def test_communication_failure(
        self: TestPowerSupplyProxySimulator,
        simulator: PowerSupplySimulator,
        component_manager: PowerSupplyProxySimulator,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test handling of communication failure between component manager and component.

        :param simulator: the power supply simulator being managed by
            this component manager.
        :param component_manager: a component manager for the component object.
        :param callbacks: dictionary of callables with asynchronous
            assertion support, for use as callbacks in testing
        """
        assert component_manager.communication_state == CommunicationStatus.DISABLED
        simulator.simulate_failure(True)

        component_manager.start_communicating()

        callbacks["communication_status"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_status"].assert_not_called()
        assert (
            component_manager.communication_state == CommunicationStatus.NOT_ESTABLISHED
        )

        component_manager.stop_communicating()
        callbacks["communication_status"].assert_call(CommunicationStatus.DISABLED)
        assert component_manager.communication_state == CommunicationStatus.DISABLED

        simulator.simulate_failure(False)
        component_manager.start_communicating()
        callbacks["communication_status"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_status"].assert_call(CommunicationStatus.ESTABLISHED)
        callbacks["communication_status"].assert_not_called()
        assert component_manager.communication_state == CommunicationStatus.ESTABLISHED

        simulator.simulate_failure(True)
        callbacks["communication_status"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        assert (
            component_manager.communication_state == CommunicationStatus.NOT_ESTABLISHED
        )

        component_manager.start_communicating()

        component_manager.stop_communicating()
        callbacks["communication_status"].assert_call(CommunicationStatus.DISABLED)
        assert component_manager.communication_state == CommunicationStatus.DISABLED

    def test_commands(
        self: TestPowerSupplyProxySimulator,
        simulator: PowerSupplySimulator,
        component_manager: PowerSupplyProxySimulator,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component manager can execute basic commands on its component.

        :param simulator: the power supply simulator being managed by
            this component manager.
        :param component_manager: a component manager for the component
            object.
        :param callbacks: dictionary of callables with asynchronous
            assertion support, for use as callbacks in testing
        """
        assert component_manager.communication_state == CommunicationStatus.DISABLED

        with pytest.raises(
            ConnectionError,
            match="Communication with component is not established",
        ):
            component_manager.off()

        with pytest.raises(
            ConnectionError,
            match="Communication with component is not established",
        ):
            component_manager.on()

        component_manager.start_communicating()
        callbacks["communication_status"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED
        )
        callbacks["communication_status"].assert_call(CommunicationStatus.ESTABLISHED)
        assert component_manager.communication_state == CommunicationStatus.ESTABLISHED

        callbacks["component_state"].assert_call(
            power=PowerState.OFF,
            fault=False,
        )

        component_manager.on(callbacks["task"])
        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED, result=(ResultCode.OK, "Command completed")
        )

        callbacks["component_state"].assert_call(
            power=PowerState.ON,
        )

        component_manager.off()

        callbacks["component_state"].assert_call(
            power=PowerState.OFF,
        )

        simulator.simulate_power(PowerState.NO_SUPPLY)
        callbacks["component_state"].assert_call(
            power=PowerState.NO_SUPPLY,
        )

        component_manager.on()
        # Poll will fail, state will not change
        callbacks["component_state"].assert_not_called()

        component_manager.off()
        # Poll will fail, state will not change
        callbacks["component_state"].assert_not_called()

        simulator.simulate_power(PowerState.UNKNOWN)
        callbacks["component_state"].assert_call(
            power=PowerState.UNKNOWN,
        )

        component_manager.on()
        # Poll will fail, state will not change
        callbacks["component_state"].assert_not_called()

        component_manager.off()
        # Poll will fail, state will not change
        callbacks["component_state"].assert_not_called()

        simulator.simulate_power(PowerState.OFF)
        callbacks["component_state"].assert_call(
            power=PowerState.OFF,
        )

        component_manager.on()

        callbacks["component_state"].assert_call(
            power=PowerState.ON,
        )
        # Test command Supersession be executing 3 commands
        # quickly
        component_manager.off(callbacks["task"])
        component_manager.on(callbacks["task"])
        component_manager.off(callbacks["task"])

        # First 2 will be Superseded
        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(
            status=TaskStatus.ABORTED,
            result=(ResultCode.ABORTED, "Superseded by later command."),
        )
        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(
            status=TaskStatus.ABORTED,
            result=(ResultCode.ABORTED, "Superseded by later command."),
        )

        # Final will run to completion.
        callbacks["task"].assert_call(status=TaskStatus.QUEUED)
        callbacks["task"].assert_call(status=TaskStatus.IN_PROGRESS)
        callbacks["task"].assert_call(
            status=TaskStatus.COMPLETED, result=(ResultCode.OK, "Command completed")
        )
