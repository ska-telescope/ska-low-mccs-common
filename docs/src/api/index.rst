===
API
===

.. toctree::
  :caption: Common modules
  :maxdepth: 2

  Component subpackage<component/index>
  Testing subpackage<testing/index>
  Device proxy<device_proxy>
  Health<health>
  Mode inheritance<mode_inheritance>
  Event serialiser<event_serialiser>
  Communication Manager<communication_manager>
  Utils<utils>
