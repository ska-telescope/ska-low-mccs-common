# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements an EventSerialiser class for managing events."""

__all__ = ["EventSerialiser"]

import collections
import queue
import threading
from datetime import datetime
from logging import Logger
from typing import Any, Callable, Optional

import tango

# device_name, event_name, event_value, event_quality, callback
EventQueueItemType = tuple[str, str, Any, tango.AttrQuality, Callable]

# device_name, event_name, event_value, event_quality, callback_name, timestamp
EventHistoryItemType = tuple[str, str, Any, str, str, str]


class EventSerialiser:
    """
    A manager class that serialises events and their callbacks in a queue.

    This class is intended to be used by parent devices that receive events
    from child devices. The parent device can queue events and their callbacks
    to be processed in a separate worker thread. This allows the parent device
    to ensure serialised executions of callbacks without blocking Tango.
    """

    def __init__(self, logger: Optional[Logger] = None) -> None:
        """
        Create a new EventSerialiser instance.

        :param logger: The logger to use for logging.
        """
        self.logger: Optional[Logger] = logger
        self._event_queue: queue.Queue[Optional[EventQueueItemType]] = queue.Queue()
        self._event_history: collections.deque[
            EventHistoryItemType
        ] = collections.deque(maxlen=50)
        self._stop_event = threading.Event()

        self._worker_thread = threading.Thread(
            target=self._process_events,
            name="EventSerialiser",
            daemon=True,
        )
        self._worker_thread.start()

    def _process_events(self) -> None:
        """Process events from the queue in a separate thread."""
        while True:
            item = self._event_queue.get()
            try:
                if item is None or self._stop_event.is_set():
                    break
                device_name, event_name, event_value, event_quality, callback = item
                callback(event_name, event_value, event_quality)
                self._update_event_history(*item)
            except Exception:  # pylint: disable=broad-except
                if self.logger:
                    self.logger.error(
                        f"Error processing event: {item}",
                        exc_info=True,
                    )
            finally:
                self._event_queue.task_done()

    # pylint: disable=too-many-arguments
    def queue_event(
        self,
        device_name: str,
        event_name: str,
        event_value: Any,
        event_quality: tango.AttrQuality,
        callback: Callable,
    ) -> None:
        """
        Queue an event and its callback to be processed in a separate thread.

        :param device_name: The name of the device that generated the event.
        :param event_name: The name of the event.
        :param event_value: The value of the event.
        :param event_quality: The quality of the event.
        :param callback: The callback to call to process the event.
        """
        self._event_queue.put(
            (device_name, event_name, event_value, event_quality, callback)
        )

    def _update_event_history(
        self,
        device_name: str,
        event_name: str,
        event_value: Any,
        event_quality: tango.AttrQuality,
        callback: Callable,
    ) -> None:
        """
        Update the event history with the event and its callback.

        :param device_name: The name of the device that generated
            the event.
        :param event_name: The name of the event.
        :param event_value: The value of the event.
        :param event_quality: The quality of the event.
        :param callback: The callback which was called to process the event.
        """
        self._event_history.appendleft(
            (
                device_name,
                event_name,
                event_value,
                event_quality.name,
                callback.__name__,
                datetime.now().isoformat(sep=" "),
            )
        )

    def clear_event_history(self) -> None:
        """Clear the event history."""
        self._event_history.clear()

    @property
    def event_history(self) -> list[EventHistoryItemType]:
        """
        Return a list of events and their callbacks that have been processed.

        :return: A list of tuples containing the event and its callback.
        """
        return list(self._event_history)

    def stop(self) -> None:
        """
        Signal the worker to exit, then wait until it finishes.

        Putting a None in the queue unblocks '_process_events' if it's waiting.
        """
        self._stop_event.set()
        self._event_queue.put(None)
        self._worker_thread.join()
