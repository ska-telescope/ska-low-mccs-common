# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements an CommunicationManager class for managing communication."""

__all__ = ["CommunicationManager"]

import threading
from concurrent.futures import ThreadPoolExecutor
from copy import copy
from logging import Logger
from typing import Callable, Mapping

from ska_control_model import CommunicationStatus

from ska_low_mccs_common.component import DeviceComponentManager


# pylint: disable=too-many-instance-attributes
class CommunicationManager:
    """
    A class for use in devices that need to establish communication with other devices.

    This class manages the communcation status for the desired proxies and
    provides a callback to notify the device when the overall communication status
    changes.
    """

    def __init__(
        self,
        communication_status_callback: Callable[[CommunicationStatus], None],
        component_status_callback: Callable[..., None],
        logger: Logger,
        *devices: Mapping[str, DeviceComponentManager],
    ) -> None:
        """
        Create a new CommunicationManager instance.

        :param communication_status_callback: The callback to notify
            the device when the overall communication status changes.
        :param component_status_callback: The callback to notify the
            device that component state has changed when communications
            are disabled.
        :param logger: The logger to use for logging.
        :param devices: The devices to manage.
        """
        self._device_pool: dict[str, DeviceComponentManager] = {}
        for device in devices:
            self._device_pool.update(device)
        self._device_communication_status: dict[
            str, CommunicationStatus
        ] = dict.fromkeys(self._device_pool.keys(), CommunicationStatus.DISABLED)
        self._desired_communication_status = CommunicationStatus.DISABLED
        self._aggregate_communication_status = CommunicationStatus.DISABLED
        self._communication_status_callback = communication_status_callback
        self._component_status_callback = component_status_callback
        self._logger = logger
        self._executor = ThreadPoolExecutor(
            max_workers=1, thread_name_prefix="CommunicationManager"
        )
        self._desired_communication_achieved = threading.Event()

    def start_communicating(self) -> None:
        """Start communicating with the devices."""
        self._executor.submit(self._start_communicating)

    def _start_communicating(self) -> None:
        if self._aggregate_communication_status == CommunicationStatus.ESTABLISHED:
            return
        self._desired_communication_status = CommunicationStatus.ESTABLISHED
        if not self._device_pool:
            self._evaluate_communication_status()
        for device in self._device_pool.values():
            device.start_communicating()

        if not self._desired_communication_achieved.wait(30):
            self._logger.error(
                "CommunicationManager failed to establish communication with all"
                "devices."
            )
        self._desired_communication_achieved.clear()

    def stop_communicating(self) -> None:
        """Stop communicating with the devices."""
        self._executor.submit(self._stop_communicating)

    def _stop_communicating(self) -> None:
        if self._aggregate_communication_status == CommunicationStatus.DISABLED:
            return
        self._desired_communication_status = CommunicationStatus.DISABLED
        if not self._device_pool:
            self._evaluate_communication_status()
        for device in self._device_pool.values():
            device.stop_communicating()

        if not self._desired_communication_achieved.wait(30):
            self._logger.error(
                "CommunicationManager failed to disable communication with all"
                "devices."
            )
        self._desired_communication_achieved.clear()

    def replace_device_pool(
        self, *new_devices: Mapping[str, DeviceComponentManager]
    ) -> None:
        """
        Replace the current device pool with another.

        If we are currently communicating with the current devices, we will
        stop communicating with them and start communicating with the new devices.

        :param new_devices: The new devices to manage.
        """
        old_device_pool = copy(self._device_pool)
        self._device_pool = {}
        for new_device in new_devices:
            self._device_pool.update(new_device)
        self._device_communication_status = dict.fromkeys(
            self._device_pool.keys(), CommunicationStatus.DISABLED
        )
        self._aggregate_communication_status = CommunicationStatus.DISABLED
        for device in old_device_pool.values():
            device.stop_communicating()
        if self._desired_communication_status == CommunicationStatus.ESTABLISHED:
            self.start_communicating()

    def update_communication_status(
        self, device_trl: str, status: CommunicationStatus
    ) -> None:
        """
        Update the communication status for a device.

        This method should be passed as a callback to the DeviceComponentManager.

        :param device_trl: The TRL of the device.
        :param status: The new communication status.
        """
        if device_trl not in self._device_communication_status:
            self._logger.debug(
                f"Device {device_trl} not in device pool. Likely the device "
                "pool has just been replaced, ignoring update."
            )
            return
        self._device_communication_status[device_trl] = status
        self._evaluate_communication_status()

    def _evaluate_communication_status(self) -> None:
        """Evaluate an aggregate status and call the callback if changed."""
        statuses = set(self._device_communication_status.values())
        for status in [CommunicationStatus.DISABLED, CommunicationStatus.ESTABLISHED]:
            if statuses == {status} and status == self._desired_communication_status:
                new_status = status
                break
        else:
            if not statuses:
                new_status = self._desired_communication_status
            else:
                new_status = CommunicationStatus.NOT_ESTABLISHED

        if new_status != self._aggregate_communication_status:
            self._aggregate_communication_status = new_status
            if self._aggregate_communication_status == CommunicationStatus.DISABLED:
                self._component_status_callback(power=None, fault=None)
            self._communication_status_callback(new_status)
            if (
                self._aggregate_communication_status
                == self._desired_communication_status
            ):
                self._desired_communication_achieved.set()
