# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements a functionality for component managers in MCCS."""
from __future__ import annotations  # allow forward references in type hints

from typing import Callable, Optional, Protocol

from ska_control_model import CommunicationStatus, ResultCode
from ska_tango_base.base import BaseComponentManager


class MccsComponentManagerProtocol(Protocol):
    """
    Specification of the interface of an MCCS component manager (for type-checking).

    Classes that provide this interface are considered to be an MCCS
    component manager even if they don't inherit from
    MccsBaseComponentManager. (e.g. the SwitchingComponentManager implements
    a passthrough mechanism that makes it an MCCS component manager even
    though it doesn't inherit from MccsBaseComponentManager).
    """

    @property
    def communication_state(
        self: MccsComponentManagerProtocol,
    ) -> CommunicationStatus:
        """Return the status of communication with the component."""

    def start_communicating(self: MccsComponentManagerProtocol) -> None:
        """Establish communication with the component."""

    def stop_communicating(self: MccsComponentManagerProtocol) -> None:
        """Break off communicating with the component."""

    def off(self: MccsComponentManagerProtocol) -> ResultCode:
        """Turn off the component."""

    def standby(self: MccsComponentManagerProtocol) -> ResultCode:
        """Put the component into standby mode."""

    def on(self: MccsComponentManagerProtocol) -> ResultCode:
        """Turn on the component."""

    def reset(self: MccsComponentManagerProtocol) -> ResultCode:
        """Reset the component."""


# pylint: disable-next=abstract-method
class MccsBaseComponentManager(BaseComponentManager):
    """The base component manager for MCCS."""

    # TODO: At present the only additional behaviour this provides is the
    # ability to set callbacks after initialisation. This is needed by the
    # ComponentManagerWithUpstreamPowerSupply, which receives its subservient
    # component managers by dependency injection, but needs to be able to claim
    # the callbacks of those component managers.
    # This is fundamental behaviour that should be implemented into
    # ska_tango_base.base.BaseComponentManager.

    def set_communication_state_callback(
        self: MccsBaseComponentManager,
        communication_state_callback: Optional[Callable[[CommunicationStatus], None]],
    ) -> None:
        """
        Set the callback to be called when communication status changes.

        :param communication_state_callback: the callback to be called
            when status of communication with the component changes. If
            None, no callback will be called.
        """
        self._communication_state_callback = communication_state_callback

    def set_component_state_callback(
        self: MccsBaseComponentManager,
        component_state_callback: Optional[Callable[..., None]],
    ) -> None:
        """
        Set the callback to be called when component state changes.

        :param component_state_callback: the callback to be called
            when component state changed. If None, no callback will
            be called.
        """
        self._component_state_callback = component_state_callback
