# Version History

## 1.3.0

* THORN-86: Add CommunicationManager. This is a class intended to be used by parent devices who wish to establish,
maintain and report communication to some set of subdevices.

## 1.2.1

* THORN-85: Add EventSerialiser as optional argument to ObsDeviceComponentManager

## 1.2.0

* THORN-85: Add EventSerialiser instance to MccsBaseDevice before init_device()
* THORN-83: Add EventSerialiser instance to MccsBaseDevice. It is currently handed out to the ModeInheritor, and is
left to subclasses to pass it do their component manager.
* THORN-81: Add EventSerialiser. This is an optional class handed to DeviceComponentManager which handles
executing callbacks in a queue, rather than immediately when the event is received from the underlying Tango
device proxy.

## 1.1.1

* THORN-18: Add default for ParentTRL

## 1.1.0

* THORN-18: Add SetParentTRL and ResetParentTRL methods on the MccsBaseDevice class

## 1.0.0

* THORN-17: 1.0.0 release - all MCCS repos

## 0.19.0

* THORN-27: Update simulated stations with new keys.

## 0.18.1

* THORN-20: Put DeviceComponentManager.stop_communicating() calls into a thread.

## 0.18.0

* THORN-20: Add MccsBaseDevice for mode inheritance.

## 0.17.0

* MCCS-2256: Removed cabinet banks

## 0.16.1

* SKB-719: Add default_args to MccsCommandProxy

## 0.16.0

* Relax PyTango version specifier to allow migration to PyTango 10.0 in downstream packages
* Fix incorrect templating of Telmodel

## 0.15.6

* THORN-39: Correct race condition when stop_communicating called shortly after start_communicating.

## 0.15.5

* SKB-635: Revert weakref.proxy changes

## 0.15.4

* SKB-635: Use weakref.proxy instead of weakref.WeakMethod in DeviceComponentManager

## 0.15.3

* THORN-46: Fix RuntimeError in MccsDeviceProxy when unsubscribing

## 0.15.2

* THORN-37: Set DeviceProxy source to DEV in start_communicating

## 0.15.1

* SKB-635: Fix garbage collection in DeviceComponentManager

## 0.15.0

* MCCS-1329: Add more detailed status information to HardwareClient.
* MCCS-2303: Improve MCCS HealthState rollup and aggregation: PowerState

## 0.14.1

* MCCS-2278: Add `lock_power_state` decorator to `utils`.

## 0.14.0

* MCCS-2140: Allow PowerState.STANDBY devices to have a HealthState
* MCCS-2284: Add MccsCompositeCommandProxy.

## 0.13.2

* MCCS-2284: Correct return type error. result_code was being interpreted as a task_status.

## 0.13.1

* MCCS-2277 - Add MccsDeviceProxy.unsubscribe_all_events()

## 0.13.0

* MCCS-2276 - Update ComponentManagerWithUpstreamPowerSupply to evaluate an UNKNOWN component together with the PowerSupply state.

## 0.12.1

* Add tmdata schema validation
* Update tmdata to v0.8.0
* Add a NetworkAttachmentDefinition to RAL.
* MCCS-2228 - Add properties to MockDeviceBuilder.

## 0.12.0

* MCCS-2141 - Update ska-tango-base to 1.0.0

## 0.11.1

* MCCS-2213 - Consolidate platform specs from other repos in this repo.

## 0.11.0

* MCCS-2215 - Handle subservient devices in ALARM state
* SKB-433 - Update MockDevice to be called with arguments

## 0.10.2

* MCCS-1524 - Adjustments to HealthModel

## 0.10.1

* MCCS-2116 - Update command_inout on mock_device to return Future object on green_mode.Futures.
* MCCS-2089 - Add PyPi to Poetry sources

## 0.10.0

* MCCS-1999 - Add LRC handling in MccsCommandProxy.
* MCCS-2000 - Bump ska-tango-base to 0.20.0
* MCCS-1969 - Correct kwarg for task_callback

## 0.9.1

* MCCS-1699 - Dependency update including ska-tango-base 0.19.1 with pytango 9.4.2 bugfix

## 0.9.0

* MCCS-1693 - Update to pytango 9.4.2
* MCCS-1691 - Fix release bug
* MCCS-1686 - Update ska-low-mccs-k8s-test-runner to have a sensible default tango host
* MCCS-1674 - Add a ska-low-mccs-k8s-test-runner OCI image.

## 0.8.2

* MCCS-1659 - Add optional volume to mount in ska-low-mccs-k8s-test-runner

## 0.8.1

* MCCS-1636 - Use ska-ser-sphinx-theme for documentation
* MCCS-1649 - Fix bug in power state handling such that @check_on decorator works
* MCCS-1485 - Allow MccsDeviceProxy to unsubscribe from events

## 0.8.0

* MCCS-1354 - Address tech debt by updating component_state_callback to be kwarg based instead of dict based
* Remove MccsComponentManager and refactor

## 0.7.2

* MCCS-1498 - Modify default thresholds in HealthRules to allow empty derived HealthModel to be initialised

## 0.7.1

* MCCS-1496 - Use default thresholds in HealthRules initialiser

## 0.7.0

* MCCS-1489 - Add HealthRules class

## 0.6.1

* MCCS-1487 - Relax ska-tango-testing dependency

## 0.6.0

* MCCS-1449 - Refactor health model

## 0.5.0

* MCCS-1424 - Update to ska-tango-base v0.18.0 for JSON validation support

## 0.4.3

* MCCS-1331 - Dependency update (including pytango 9.3.6)

## 0.4.2

* MCCS-1225 - Update runner pod

## 0.4.0

* MCCS-1305 - asynchronous power supply proxy simulator
* MCCS-1302 - Reconfigure the test directory structure of ska-low-mccs-common to the ska standard
* MCCS-1298 - use ska-tango-testing

## 0.3.0

* MCCS-1294 - Fix a few static type checks in ska-low-mccs-common and release patch
* MCCS-1293 - Make WebHardwareClient connectionless

## 0.2.0

* MCCS-1195 - k8s-test-runner helm chart

## 0.1.3

* MCCS-1150 - inalise linting, tidy and reinstate the max line length=88 in the ska-low-mccs-common repository
* MCCS-1144 - Better error message reporting on check_communicating decorator

## 0.1.2

* LOW-362 - Use ska-tango-testing for DeviceProxy MDTC patch

## 0.1.1

* MCCS-1140 - add extra-index-url

## 0.1.0

* MCCS-1133 - initial creation
