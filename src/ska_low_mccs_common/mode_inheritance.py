# -*- coding: utf-8 -*-
#
# This file is part of the SKA Tango Base project
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.
"""This module implements mode inherition."""

import logging
import threading
from typing import Any, Callable, Final, Optional

import tango
from ska_control_model import AdminMode  # , ControlMode, SimulationMode, TestMode

from ska_low_mccs_common.device_proxy import MccsDeviceProxy
from ska_low_mccs_common.event_serialiser import EventSerialiser


class CallbackManager:
    """
    This class implements a CallbackManager for modes.

    It implements a cache of mode values from a parent
    device, and is responsible for calling callbacks
    whenever there is a change.

    Create a `CallbackManager` object,
    feed it updates on the modes of a parent,
    and let it know when it is in 'active' mode and
    should therefore call its callbacks.

    Call the `update` method to tell it that the parent
    device has changed a mode; for example,
    `callback_manager.update("adminMode", AdminMode.ONLINE)`.
    The stored cache will remember the mode that has been set,
    but it will only call the relevant callback if/once its
    `active` attribute is set to `True`.

    You can set the `active` property at any time,
    by simply writing a boolean value to it;
    for example `callback_manager.active = True`.

    Consider this scenario:

    1. An `MccsStation` instance is set to `inherit` its `adminMode` from its parent,
       which is the `MccsController`. The `MccsController` has `adminMode` `OFFLINE`,
       so this `MccsStation` instance also adopts `adminMode` `OFFLINE`.

    2. The `MccsController` has its `adminMode` changed to `ONLINE`.
       This change propagates through the entire MCCS subsystem,
       including this `MccsStation`, because its `inherit` attribute is set to `True`.

    3. A decision is made to make this station an "engineering station" for a time.
       That means it needs to be excised from the rest of the array,
       and its `adminMode` needs to be set to `ENGINEERING`.
       This is achieved by explicitly overriding the device's `adminMode`,
       which also causes it to stop inheriting this mode from its parent device.
       (That explicit override flows down to the station's subordinate devices,
       because these subordinate devices are still set to inherit their `adminMode`s
       from their parent.
       The only point in the system where the chain of inheritance is broken,
       is at this one `MccsStation` instance.)

    4. The `MccsController` has its `adminMode` changed to `OFFLINE`.
       This change propagates through the entire MCCS subsystem,
       except for the "engineering station",
       which remains online in `ENGINEERING` `adminMode`,
       because it has been told not to inherit from its parent.

    5. The station is not longer needed for an "engineering station",
       and is ready to be included back into the overall array.
       The `MccsStation` instance is told to `inherit` its `adminMode` from its parent.
       It immediately changes `adminMode` to `OFFLINE`,
       because that is the `adminMode` of its parent device;
       and this change flows down to the `MccsStation` instance's subordinate devices.

    Note that this class is agnostic to the modes that it manages.
    """

    def __init__(
        self,
        **callbacks: Callable[[Any], None] | None,
    ) -> None:
        """
        Initialise a new instance.

        :param callbacks: keyword arguments specifying modes to be managed,
            and optionally providing a callback to be called when the mode changes.
            For example, if you initialise this with
            `CallbackManager(adminMode=self.adminModeChanged)`,
            then this `CallbackManager` instance will support a mode named `adminMode`,
            and will call `self.adminModeChanged`
            whenever it determines that the `adminMode` has changed.

            It is also possible to initialise this as
            `CallbackManager(adminMode=None)`,
            in which case this `CallbackManager` will support `adminMode`,
            but will not call a callback when `adminMode` changes.
        """
        self._lock = threading.Lock()

        self._active = False
        self._values_cache: dict[str, Any] = dict.fromkeys(callbacks)

        self._callbacks = callbacks

    @property
    def active(self) -> bool:
        """
        Whether this `CallbackManager` is currently calling its callbacks.

        :return: whether this `CallbackManager` is currently active
        """
        return self._active

    @active.setter
    def active(self, active: bool) -> None:
        """
        Set whether this `CallbackManager` is active, i.e. should call its callbacks.

        :param active: whether this `CallbackManager` should call its callbacks.
        """
        with self._lock:
            if self._active == active:
                return
            self._active = active
            if active:
                for name, value in self._values_cache.items():
                    self._set(name, value)

    def _check_name(self, name: str) -> None:
        if name not in self._values_cache:
            raise ValueError(
                f"Cannot access unknown mode '{name}': "
                f"known modes are: {list(self._values_cache.keys())}"
            )

    def update(self, name: str, value: Any) -> None:
        """
        Advise that the mode of a parent has changed.

        :param name: name of the mode
        :param value: the new value taken by the parent.
        """
        with self._lock:
            self._check_name(name)
            self._values_cache[name] = value
            if self._active:
                self._set(name, value)

    def _set(self, name: str, value: Any) -> None:
        self._values_cache[name] = value

        callback = self._callbacks[name]
        if callback:
            callback(value)


class ModeInheritor:
    """
    This class manages mode inheritance for a device.

    Initialise it with the TRL of its parent tango device,
    and callbacks for the modes that it is managing.

    You can then set the modes explicitly,
    or set the `inheriting` attribute to toggle mode inheritance.
    """

    INITIAL_VALUE_TIMEOUT: Final = 5.0

    def __init__(
        self,
        parent_trl: str | None,
        logger: logging.Logger,
        *,
        adminmode_callback: Callable[[AdminMode], None],
        # controlmode_callback: Callable[[ControlMode], None],
        # simulationmode_callback: Callable[[SimulationMode], None],
        # testmode_callback: Callable[[TestMode], None],
        event_serialiser: Optional[EventSerialiser] = None,
    ) -> None:
        """
        Initialise a new instance.

        :param parent_trl: TRL of the parent Tango device.
            If None, this device does not have a parent device,
            and therefore cannot inherit its modes.
        :param logger: a logger for this object to use.
        :param adminmode_callback: callback to be called
            whenever the adminMode value changes.
            This must be provided as an explicit keyword argument;
            i.e. `adminmode_callback=my_callback`.
            This is to ensure that support for more modes can be added
            in future without breaking backwards-compatibility.
        :param event_serialiser: an EventSerialiser instance
            for managing the events.
        """
        self._logger = logger
        self._initial_value_event = threading.Event()

        # Note the lower-case keywords
        # Tango device attribute names are case-insensitive,
        # and change events often contain a lower-case attribute name
        # and the easiest way to handle that is to convert everything to lower case
        # before passing it to the CallbackManager.
        self._callback_manager = CallbackManager(
            adminmode=adminmode_callback,
            # controlmode=controlmode_callback,
            # simulationmode=simulationmode_callback,
            # testmode=testmode_callback,
        )

        self._parent: MccsDeviceProxy | None = None
        self._finished = threading.Event()
        if parent_trl:
            self._parent = MccsDeviceProxy(
                parent_trl,
                logger=self._logger,
                connect=False,
                event_serialiser=event_serialiser,
            )
            modes = [
                "adminmode",
                # "controlmode",
                # "simulationmode",
                # "testmode",
            ]
            self._event_thread = threading.Thread(
                target=self._setup_subscriptions,
                args=[modes, parent_trl],
            )
            self._event_thread.start()

        # It's essential that device default to inheriting True,
        # but if the memorized value of inheriting is False,
        # we don't want start inheriting before that memorized value has been written.
        # To avoid that, we wait up to INITIAL_VALUE_TIMEOUT
        # for the inheriting attribute to get written;
        # and we only set it to True if that time expires.
        # The full cost of this timeout is only paid when we do a fresh deployment
        # and hence there is no memorized value in the db.
        # In normal operation there will be a memorized value,
        # so this costs us nothing.
        # In unit testing, you can avoid this timeout by explicitly setting
        # the inheriting attribute.
        def _set_initial_inherit(timeout: float) -> None:
            self._initial_value_event.wait(timeout)
            if not self._initial_value_event.is_set():
                self._callback_manager.active = True

        threading.Thread(
            target=_set_initial_inherit, args=[self.INITIAL_VALUE_TIMEOUT]
        ).start()

    def _setup_subscriptions(self, modes: list[str], parent_trl: str) -> None:
        """Connect to the parent device and setup the subscriptions.

        :param modes: the mode attributes to subscribe to.
        :param parent_trl: the parent TRL
        """
        if self._parent:
            self._parent.connect()
            for mode in modes:
                self._logger.debug(
                    f"Setting up subscription for {mode} on {parent_trl}"
                )
                self._parent.add_change_event_callback(mode, self.parent_mode_changed)

            self._finished.wait()
            self._parent.unsubscribe_all_change_events()

    def cleanup(self) -> None:
        """Notify the event thread to cleanup."""
        if self._parent:
            self._finished.set()
            self._event_thread.join()

    @property
    def inheriting(self) -> bool:
        """
        Return whether we are inheriting modes from the parent mode.

        :return: whether we are inheriting modes from the parent mode.
        """
        return self._callback_manager.active

    @inheriting.setter
    def inheriting(self, inheriting: bool) -> None:
        """
        Set whether we are inheriting modes from the parent mode or not.

        :param inheriting: whether we are to inherit modes form the parent.
        """
        self._callback_manager.active = inheriting
        self._initial_value_event.set()

    def parent_mode_changed(
        self,
        mode_name: str,
        mode_value: Any,
        event_quality: tango.AttrQuality,
    ) -> None:
        """
        Tango event handler for change of a parent mode.

        :param mode_name: name of the mode that has changed
        :param mode_value: the new mode value
        :param event_quality: the quality of the change event
        """
        self._callback_manager.update(mode_name.lower(), mode_value)
