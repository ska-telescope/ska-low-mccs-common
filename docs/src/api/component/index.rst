====================
Component subpackage
====================

.. automodule:: ska_low_mccs_common.component


.. toctree::

  Component manager<component_manager>
  Device component manager<device_component_manager>
  Hardware client<hardware_client>
  Object component manager<object_component_manager>
  Object component<object_component>
  Switching component manager<switching_component_manager>
  Upstream component manager<upstream_component_manager>
