# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains unit tests for the ska_low_mccs.device_proxy module."""
from __future__ import annotations  # allow forward references in type hints

import logging
import unittest
import unittest.mock
from concurrent import futures
from typing import Any

import pytest_mock
from ska_control_model import ResultCode
from ska_tango_testing.context import TangoContextProtocol
from tango import GreenMode

from ska_low_mccs_common import MccsDeviceProxy


# pylint: disable=too-few-public-methods
class MockBoundMethod:
    """Class for submitting a mock to the MccsDeviceProxy which uses WeakMethods."""

    def __init__(self, mock: unittest.mock.Mock) -> None:
        """
        Initialise instance.

        :param mock: mock to be called when the bound method is called.
        """
        self.mock = mock

    def method(self, *args: Any, **kwargs: Any) -> None:
        """
        Bound method to be supplied to the MccsDeviceProxy.

        This method will be called in the callbacks, we can then assert on the
        underlying mock.

        :param args: any positional arguments the callback was called with.
        :param kwargs: any keyword arguments the callback was called with.
        """
        self.mock(*args, **kwargs)


class TestMccsDeviceProxy:
    """This class contains unit tests for the MccsDeviceProxy class."""

    def test_subscription(
        self: TestMccsDeviceProxy,
        tango_harness: TangoContextProtocol,
        mocker: pytest_mock.MockerFixture,
        logger: logging.Logger,
    ) -> None:
        """
        Test change event subscription.

        Specifically, test that when a client registered a change event
        callback on an MccsDeviceProxy, this results in the underlying
        device receiving a subscribe_event call for the specified event.

        :param tango_harness: a test harness for tango devices
        :param mocker: fixture that wraps unittest.Mock
        :param logger: the logger to be used by the object under test
        """
        fqdn = "mock/mock/1"
        mccs_device_proxy = MccsDeviceProxy(fqdn, logger)

        event_count = 2  # test should pass for any positive number
        callbacks = [MockBoundMethod(mocker.Mock()).method for _ in range(event_count)]

        mock_device_proxy = tango_harness.get_device("mock/mock/1")

        for i in range(event_count):
            event_name = f"mock_event_{i}"
            mccs_device_proxy.add_change_event_callback(event_name, callbacks[i])

            # check that initialisation resulted in the device at the fqdn
            # receiving a subscription to the event
            mock_device_proxy.subscribe_event.assert_called_once()
            args, kwargs = mock_device_proxy.subscribe_event.call_args
            assert args[0] == event_name

            mock_device_proxy.reset_mock()

    def test_green_mode_command_inout(
        self: TestMccsDeviceProxy,
        tango_harness: TangoContextProtocol,
        mocker: pytest_mock.MockerFixture,
        logger: logging.Logger,
    ) -> None:
        """
        Test that a command_inout with green_mode returns a future.

        :param tango_harness: a test harness for tango devices
        :param mocker: fixture that wraps unittest.Mock
        :param logger: the logger to be used by the object under test
        """
        fqdn = "mock/mock/1"
        mccs_device_proxy = MccsDeviceProxy(fqdn, logger)

        future_result = mccs_device_proxy.command_inout(
            "MockCallable", green_mode=GreenMode.Futures, wait=True
        )

        # A simple check that this is a Future object.
        assert isinstance(future_result, futures.Future)
        assert future_result.result() == [ResultCode.OK, "Mock command completed"]

    def test_unsubscription(
        self: TestMccsDeviceProxy,
        tango_harness: TangoContextProtocol,
        mocker: pytest_mock.MockerFixture,
        logger: logging.Logger,
    ) -> None:
        """
        Test event unsubscription.

        Sets up a set of mock event subscriptions, then unsubscribes from them
        , confirms the underlying device received a unsubscribe_event all
        and confirms the subscriptions dictionary is now empty.

        :param tango_harness: a test harness for tango devices
        :param mocker: fixture that wraps unittest.Mock
        :param logger: the logger to be used by the object under test
        """
        fqdn = "mock/mock/1"
        mccs_device_proxy = MccsDeviceProxy(fqdn, logger)

        event_count = 2  # test should pass for any positive number
        callbacks = [mocker.Mock() for _ in range(event_count)]

        mock_device_proxy = tango_harness.get_device("mock/mock/1")

        for i in range(event_count):
            event_name = f"mock_event_{i}"
            mock_device_proxy.subscribe_event(
                event_name,
                None,
                callbacks[i],
                stateless=True,
            )
            mccs_device_proxy._change_event_subscription_ids[event_name.lower()] = i
            event_id = mccs_device_proxy._change_event_subscription_ids[
                event_name.lower()
            ]
            mccs_device_proxy.unsubscribe_change_event(event_name)

            mock_device_proxy.unsubscribe_event.assert_called_once()
            args, kwargs = mock_device_proxy.unsubscribe_event.call_args
            assert args[0] == event_id
            mock_device_proxy.reset_mock()

        assert len(mccs_device_proxy._change_event_subscription_ids) == 0

    def test_event_pushing(
        self: TestMccsDeviceProxy,
        tango_harness: TangoContextProtocol,
        mocker: pytest_mock.MockerFixture,
        logger: logging.Logger,
    ) -> None:
        """
        Test that events result in callbacks being called.

        Specifically, test that when an MccsDeviceProxy's
        ``_change_event_received`` callback method is called with an
        change event for a particular attribute, all callbacks
        registered with the MccsDeviceProxy for that attribute are
        called, and callbacks registered for other attributes are not
        called.

        :param tango_harness: a test harness for tango devices
        :param mocker: fixture that wraps unittest.Mock
        :param logger: the logger to be used by the object under test
        """
        event_count = 3  # test should pass for any positive number
        fqdn = "mock/mock/1"

        device_proxy = MccsDeviceProxy(fqdn, logger)

        mock_callbacks = [MockBoundMethod(mocker.Mock()) for i in range(event_count)]
        for i in range(event_count):
            event_name = f"mock_event_{i+1}"
            device_proxy.add_change_event_callback(event_name, mock_callbacks[i].method)

            for j in range(event_count):
                if i == j:
                    mock_callbacks[j].mock.assert_called_once()
                    mock_callbacks[j].mock.reset_mock()
                else:
                    mock_callbacks[j].mock.assert_not_called()

        for i in range(event_count):
            event_name = f"mock_event_{i+1}"
            event_value = f"mock_value_{i+1}"
            event_quality = f"mock_quality_{i+1}"

            mock_event = mocker.Mock()
            mock_event.err = False
            mock_event.attr_value.name = event_name
            mock_event.attr_value.value = event_value
            mock_event.attr_value.quality = event_quality

            # push the event (this is quite implementation-dependent
            # because we are pretending to be the tango device)
            device_proxy._change_event_received(mock_event)

            # check that the mock callback was called as expected, and
            # that other mock callbacks were not called.
            for j in range(event_count):
                if i == j:
                    mock_callbacks[j].mock.assert_called_once_with(
                        event_name, event_value, event_quality
                    )
                    mock_callbacks[j].mock.reset_mock()
                else:
                    mock_callbacks[j].mock.assert_not_called()
