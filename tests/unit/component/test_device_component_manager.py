# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests of the device_component_manager module."""
from __future__ import annotations

import logging
import threading
import time
import unittest.mock
from typing import Any

import pytest
import tango
from ska_control_model import AdminMode, CommunicationStatus, HealthState, PowerState
from ska_tango_base.base import check_on
from ska_tango_base.faults import ComponentError
from ska_tango_testing.context import TangoContextProtocol
from ska_tango_testing.mock import MockCallableGroup

from ska_low_mccs_common.component import DeviceComponentManager


@pytest.fixture(name="component_manager")
def component_manager_fixture(
    tango_harness: TangoContextProtocol,
    fqdn: str,
    logger: logging.Logger,
    callbacks: MockCallableGroup,
) -> DeviceComponentManager:
    """
    Return a device component manager for testing.

    :param tango_harness: a test harness for MCCS tango devices
    :param fqdn: the FQDN of the device to be managed by this component
        manager.
    :param logger: a logger for the component manager to use.
    :param callbacks: dictionary of callables with asynchronous
        assertion support, for use as callbacks in testing

    :return: a device component manager for testing.
    """
    return DeviceComponentManager(
        fqdn,
        logger,
        callbacks["communication_status"],
        callbacks["component_state"],
    )


class TestDeviceComponentManager:
    """Tests of the DeviceComponentManager class."""

    def test_communication(
        self: TestDeviceComponentManager,
        component_manager: DeviceComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component manager's communication with the device.

        :param component_manager: the component manager under test
        :param callbacks: dictionary of callables with asynchronous
            assertion support, for use as callbacks in testing
        """
        assert component_manager.communication_state == CommunicationStatus.DISABLED
        component_manager.start_communicating()
        callbacks["communication_status"].assert_call(
            CommunicationStatus.NOT_ESTABLISHED,
        )
        callbacks["communication_status"].assert_call(CommunicationStatus.ESTABLISHED)
        assert component_manager.communication_state == CommunicationStatus.ESTABLISHED

        component_manager.stop_communicating()
        callbacks["communication_status"].assert_call(CommunicationStatus.DISABLED)
        assert component_manager.communication_state == CommunicationStatus.DISABLED

    @pytest.mark.parametrize(
        ("component_manager_command", "device_command"),
        [
            ("on", "On"),
            ("standby", "Standby"),
            ("off", "Off"),
            ("reset", "Reset"),
        ],
    )
    def test_command(
        self: TestDeviceComponentManager,
        component_manager: DeviceComponentManager,
        mock_proxy: unittest.mock.Mock,
        component_manager_command: str,
        device_command: str,
    ) -> None:
        """
        Test command execution.

        :param component_manager: the component manager under test
        :param mock_proxy: a mock proxy to the component device
        :param component_manager_command: the name of the command to the
            component manager
        :param device_command: the name of the command that is expected
            to be called on the device.
        """
        with pytest.raises(
            ConnectionError,
            match="Communication with component is not established",
        ):
            getattr(component_manager, component_manager_command)()
        getattr(mock_proxy, device_command).assert_not_called()
        assert component_manager.power_state is None

        component_manager.start_communicating()
        time.sleep(0.1)

        getattr(component_manager, component_manager_command)()
        mock_command = getattr(mock_proxy, device_command)
        mock_command.assert_next_call()
        assert component_manager.power_state is not None

    def test_health(
        self: TestDeviceComponentManager,
        component_manager: DeviceComponentManager,
        callbacks: MockCallableGroup,
    ) -> None:
        """
        Test the component managers handling of health.

        :param component_manager: the component manager under test
        :param callbacks: dictionary of callables with asynchronous
            assertion support, for use as callbacks in testing
        """
        assert component_manager.health is None

        component_manager._device_admin_mode_changed(
            "adminMode", AdminMode.ONLINE, tango.AttrQuality.ATTR_VALID
        )
        assert component_manager.health == HealthState.UNKNOWN
        callbacks["component_state"].assert_call(health=HealthState.UNKNOWN)

        component_manager._device_health_state_changed(
            "healthState", HealthState.DEGRADED, tango.AttrQuality.ATTR_VALID
        )
        assert component_manager.health == HealthState.DEGRADED
        callbacks["component_state"].assert_call(health=HealthState.DEGRADED)

        component_manager._device_admin_mode_changed(
            "adminMode", AdminMode.RESERVED, tango.AttrQuality.ATTR_VALID
        )
        assert component_manager.health is None
        callbacks["component_state"].assert_call(health=None)

        component_manager._device_admin_mode_changed(
            "adminMode", AdminMode.ONLINE, tango.AttrQuality.ATTR_VALID
        )
        assert component_manager.health == HealthState.DEGRADED
        callbacks["component_state"].assert_call(health=HealthState.DEGRADED)

    def test_check_on(
        self: TestDeviceComponentManager,
        component_manager: DeviceComponentManager,
    ) -> None:
        """
        Test the component manager functions correctly with the @check_on decorator.

        :param component_manager: the component manager under test
        """
        component_manager.start_communicating()
        time.sleep(0.1)

        @check_on
        def test_function(component_manager: DeviceComponentManager) -> None:
            return

        with pytest.raises(ComponentError, match="Component is not powered ON"):
            test_function(component_manager)

        component_manager._device_state_changed(
            "state", tango.DevState.ON, tango.AttrQuality.ATTR_VALID
        )  # Pretend to receive change event from device

        assert component_manager.power_state == PowerState.ON

        test_function(component_manager)

    def test_cleanup(
        self: TestDeviceComponentManager,
        tango_harness: TangoContextProtocol,
        fqdn: str,
    ) -> None:
        """
        Test the component manager functions correctly cleans up when deleted.

        This test is verifying a bug fix whereby a circular reference was causing
        non-deterministic problems with the garbage collection, which in turn was
        causing a build up of zombie threads.

        :param tango_harness: a test harness for MCCS tango devices
        :param fqdn: the FQDN of the device to be managed by this component
            manager.
        """

        def test_function(*args: Any, **kwargs: Any) -> None:
            pass

        for _ in range(100):
            component_manager = DeviceComponentManager(
                fqdn,
                logging.Logger("my_test_logger"),
                test_function,
                test_function,
            )
            component_manager.start_communicating()
            del component_manager

        time.sleep(0.1)

        # We don't know exactly how many threads will still be executing at this point
        # as it depends when garbage collection gets to them, however if we assert that
        # far less than 100 threads are still executing, we can be happy that they are
        # being collected correctly.
        assert len(threading.enumerate()) <= 10
