# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains pytest test harness for tests of the MCCS component module."""
from __future__ import annotations

import logging
import unittest.mock
from typing import Callable

import pytest
import pytest_mock
from ska_control_model import ResultCode

from ska_low_mccs_common import MccsDeviceProxy
from ska_low_mccs_common.testing.mock import MockDeviceBuilder


@pytest.fixture()
def mock_factory() -> MockDeviceBuilder:
    """
    Fixture that provides a mock factory for device proxy mocks.

    This default factory provides mocks with some basic behaviours of
    base devices. i.e. on, off, standby, etc commands.

    :return: a factory for device proxy mocks
    """
    builder = MockDeviceBuilder()
    builder.add_result_command("Off", result_code=ResultCode.QUEUED)
    builder.add_result_command("Standby", result_code=ResultCode.OK)
    builder.add_result_command("On", result_code=ResultCode.QUEUED)
    builder.add_result_command("Reset", result_code=ResultCode.OK)
    return builder


@pytest.fixture(name="fqdn")
def fqdn_fixture() -> str:
    """
    Return an FQDN for a mock device.

    :return: an FQDN for a mock device.
    """
    return "mock/mock/1"


@pytest.fixture()
def mock_proxy(fqdn: str, logger: logging.Logger) -> MccsDeviceProxy:
    """
    Return a mock proxy to the provided FQDN.

    :param fqdn: the FQDN of the device.
    :param logger: a logger for the device proxy.

    :return: a mock proxy to the device with the FQDN provided.
    """
    return MccsDeviceProxy(fqdn, logger)


@pytest.fixture()
def mock_component_manager_factory(
    mocker: pytest_mock.MockerFixture,
) -> Callable[[], unittest.mock.Mock]:
    """
    Return a factory that can be called to provide a mock component manager.

    :param mocker: fixture that wraps unittest.mock

    :return: a mock component manager factory
    """
    return mocker.Mock
