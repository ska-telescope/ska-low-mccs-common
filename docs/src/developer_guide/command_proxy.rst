####################################
Developer guide to MccsCommandProxy.
####################################

This guide is aimed towards developers of MCCS looking to implement 
the MccsCommandProxy or MccsCompositeCommandProxy.

****************
MccsCommandProxy
****************

The ``MccsCommandProxy`` is used to invoke a command on a device,
and monitor its progress, simply by invoking a command on a proxy,
and passing it a task_callback. The command proxy interacts with the device interface,
hiding its details from the user, and calls the task_callback as appropriate.

   .. code-block:: python

    def configure(
        self: FieldStationComponentManager,
        task_callback: Optional[Callable] = None,
        **kwargs: Any,
    ) -> tuple[TaskStatus, str]:
        """
        Configure the field station.

        Currently this only supports configuring FNDH alarm thresholds.

        :param task_callback: callback to be called when the status of
            the command changes
        :param kwargs: keyword arguments extracted from the JSON string.

        :return: the task status and a human-readable status message
        """
        command_proxy = MccsCommandProxy(self._fndh_name, "Configure", self.logger)
        return command_proxy(json.dumps(kwargs), task_callback=task_callback)


*************************
MccsCompositeCommandProxy
*************************

The ``MccsCompositeCommandProxy`` is a collection of MccsCommandProxys.
This is a blocking function and will wrap up the results of a MccsCommandProxy
using a number of ``CommandRules`` to evaluate against. If you choose to 
supply no ``CommandRules`` the ``MccsCompositeCommandProxy`` will return 
ResultCode.FAILED unless all the MccsCommandProxys return ResultCode.OK.

For example, if you are looking at implementing MccsStation.On(). As a developer 
you know this will call FieldStation.On() and SpsStation.On(). You may want to allow
the possible return (``ResultCode.REJECTED``, "Device is already in ON state.") as an
allowed reply. The CommandRule.allowed_result is an optional list containing a 
tuple[tuple[ResultCode, str]]. Sometimes the exact string will be dynamic, as a 
result we will check that this string is a subset of the command result.


   .. code-block:: python

    def _on(
        self: StationComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Turn on this station.

        The order to turn a station on is: FieldStation, then tiles and
        antennas.

        :param task_callback: Update task state, defaults to None
        :param task_abort_event: Abort the task
        """
        composite_on = MccsCompositeCommandProxy(self.logger)
        rules = [
            CommandRule(
                self._field_station_trl,
                "On",
                [(ResultCode.REJECTED, "Device is already in ON state.")],
            )
        ]
        composite_on += MccsCommandProxy(self._field_station_trl, "On", self.logger)
        composite_on += MccsCommandProxy(self._sps_station_trl, "On", self.logger)
        result_evaluator = CompositeCommandResultEvaluator(rules)
        composite_on(task_callback=task_callback, command_evaluator=result_evaluator, timeout=600)


****************
Wrap up message.
****************

One of the main benefits of the MccsCompositeCommandProxy is its ability to 
automaticatically wrap up the results of subcommands to help diognose when issue 
arise. If any of the subcommands return anything but ResultCode.OK this will be wrapped
up in the message. It may be that this failure was allowed by the ``CommandRules``, but it
supplies the user with information about what actually happened. As an example if a user 
attempts to turn on the MccsController device with a FieldStation and SpsStation OFFLINE, 
the result will be as follow (note: this exact result may differ depending on version.)

   .. code-block:: python

    [result_code],[command_id] = control.on()

    control.checklongrunningcommandstatus(ci)
    'COMPLETED'

    val =json.loads(json.loads(control.longrunningcommandresult[1])[-1])
    print(json.dumps(val, indent=4))
    {
        "low-mccs/cabinetbank/cb-1/On": {
            "action": "low-mccs/cabinetbank/cb-1/On",
            "result_code": "FAILED",
            "result_info": {
                "low-mccs/station/ci-1/On": {
                    "action": "low-mccs/station/ci-1/On",
                    "result_code": "FAILED",
                    "result_info": {
                        "low-mccs/fieldstation/ci-1/On": {
                            "action": "low-mccs/fieldstation/ci-1/On",
                            "result_code": "REJECTED",
                            "result_info": "Device is already in ON state."
                        },
                        "low-mccs/spsstation/ci-1/On": {
                            "action": "low-mccs/spsstation/ci-1/On",
                            "result_code": "FAILED",
                            "result_info": "Error invoking command on device low-mccs/spsstation/ci-1: DevFailed(args = (DevError(desc = 'Command On not allowed when the device is in DISABLE state', origin = 'CORBA::Any* Tango::DeviceClass::command_handler(Tango::DeviceImpl*, const string&, const CORBA::Any&) at (/src/cppTango/src/server/deviceclass.cpp:1210)', reason = 'API_CommandNotAllowed', severity = tango._tango.ErrSeverity.ERR), DevError(desc = 'Failed to execute command_inout on device low-mccs/spsstation/ci-1, command On', origin = 'virtual Tango::DeviceData Tango::Connection::command_inout(const string&, const Tango::DeviceData&) at (/src/cppTango/src/client/devapi_base.cpp:1334)', reason = 'API_CommandFailed', severity = tango._tango.ErrSeverity.ERR)))."
                        }
                    }
                },
                "low-mccs/station/ci-2/On": {
                    "action": "low-mccs/station/ci-2/On",
                    "result_code": "FAILED",
                    "result_info": {
                        "low-mccs/fieldstation/ci-2/On": {
                            "action": "low-mccs/fieldstation/ci-2/On",
                            "result_code": "FAILED",
                            "result_info": "Error invoking command on device low-mccs/fieldstation/ci-2: DevFailed(args = (DevError(desc = 'Command On not allowed when the device is in DISABLE state', origin = 'CORBA::Any* Tango::DeviceClass::command_handler(Tango::DeviceImpl*, const string&, const CORBA::Any&) at (/src/cppTango/src/server/deviceclass.cpp:1210)', reason = 'API_CommandNotAllowed', severity = tango._tango.ErrSeverity.ERR), DevError(desc = 'Failed to execute command_inout on device low-mccs/fieldstation/ci-2, command On', origin = 'virtual Tango::DeviceData Tango::Connection::command_inout(const string&, const Tango::DeviceData&) at (/src/cppTango/src/client/devapi_base.cpp:1334)', reason = 'API_CommandFailed', severity = tango._tango.ErrSeverity.ERR)))."
                        },
                        "low-mccs/spsstation/ci-2/On": {
                            "action": "low-mccs/spsstation/ci-2/On",
                            "result_code": "REJECTED",
                            "result_info": "Device is already in ON state."
                        }
                    }
                }
            }
        }
    }


