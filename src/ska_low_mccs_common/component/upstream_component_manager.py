# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains classes for interacting with upstream devices."""
from __future__ import annotations

import logging
import threading
import time
from typing import Any, Callable, Optional

from ska_control_model import CommunicationStatus, PowerState, ResultCode, TaskStatus
from ska_tango_base.base import check_communicating
from ska_tango_base.executor import TaskExecutorComponentManager
from ska_tango_base.poller import PollingComponentManager

from ..component.component_manager import MccsBaseComponentManager

__all__ = ["PowerSupplySimulator", "PowerSupplyProxySimulator"]


class PowerSupplySimulator:
    """
    A power supply simulator for the component manager to manage.

    It supports the four power states:

    * NO_SUPPLY means the power supply is itself unsupplied with power,
      so it cannot be communicated with, and hence cannot service the
      :py:meth:`.off` or :py:meth:`.on` commands.

    * UNKNOWN means the power supply is supplied with power, but
      communication with it has not been established. Again, the power
      supply cannot service `off` or `on` commands.

    * OFF means the power supply is supplied with power, and therefore
      able to supply power. However it is currently *not* supplying
      power. Communication with the power supply is established, so it
      can accept an `on` command.

    * ON means the power supply is supplied with power, and is therefore
      able to supply power, and indeed is currently doing so.
      Communication with the power supply is established, so it can
      accept an `off` command.

    The main commands are `off` and `on`, which simulate time-consuming
    operations on the power supply; and a general `simulate_power`
    command which causes the power supply to "spontaneously" change to
    a new power state.
    """

    def __init__(
        self: PowerSupplySimulator,
        initial_power_state: PowerState = PowerState.OFF,
        initial_fail: bool = False,
        time_to_return: float = 0.05,
        time_to_complete: float = 0.4,
    ) -> None:
        """
        Initialise a new instance.

        :param initial_power_state: the initial power state of this
            simulator.
        :param initial_fail: whether to enter a simulated failure state.
        :param time_to_return: the amount of time to delay
            before returning from a command method.
            This simulates unavoidable communications latency.
        :param time_to_complete: the amount of time to delay
            before the component state reflects actions taken on it.
            This simulates hardware processing delay.
        """
        self._power_state = initial_power_state
        self._fail = initial_fail
        self._time_to_return = time_to_return or 0
        self._time_to_complete = time_to_complete or 0

        self._lock = threading.Lock()

    def _simulate_power_operation(
        self: PowerSupplySimulator,
        is_on: bool,
    ) -> None:
        """
        Simulate a change in power state.

        This simulates a spontaneous change in power state i.e. the
        power state changes without the :py:meth:`.off` or
        :py:meth:`.on` commands having been issued.

        The `off` and `on` commands can only be used when the power
        supply is itself supplied with power, and a connection to it is
        established. This method can also be used to simulate the case
        of a power supply that is itself not supplied with power (i.e.
        NO_SUPPLY) and so cannot be turned off or on;l or the case
        where there is no connection to the power supply (UNKNOWN).

        :param is_on: if True, simulate power on; otherwise, simulate
            power off.

        :raises ValueError: if the power supply simulator has no power supply.
        """
        # Simulate the unavoidable synchronous latency cost of communicating with this
        # component.
        time.sleep(self._time_to_return)

        if self._power_state == PowerState.NO_SUPPLY:
            raise ValueError("This power supply simulator has no power supply.")
        if self._power_state == PowerState.UNKNOWN:
            raise ValueError("No communication with power supply.")

        # Kick off asynchronous processing, then return immediately.
        def _async_simulate_power_operation() -> None:
            with self._lock:
                time.sleep(self._time_to_complete)
                self._power_state = PowerState.ON if is_on else PowerState.OFF

        threading.Thread(target=_async_simulate_power_operation).start()

    def off(self: PowerSupplySimulator) -> None:
        """Deny power to the downstream device."""
        self._simulate_power_operation(False)

    def on(self: PowerSupplySimulator) -> None:
        """Supply power to the downstream device."""
        self._simulate_power_operation(True)

    def simulate_power(self, power_state: PowerState) -> None:
        """
        Simulate a specific power state.

        :param power_state: the new power state.
        """
        self._power_state = power_state

    def simulate_failure(self, fail: bool) -> None:
        """
        Simulate, or stop simulating, power supply failure.

        When simulating failure, any attempt to read the
        :py:attr:`power_state` property will raise an exception.

        :param fail: whether to simulate failure.
        """
        self._fail = fail

    @property
    def power_state(self: PowerSupplySimulator) -> PowerState:
        """
        Return whether the power supply is on or not.

        :raises ValueError: if simulating failure.

        :return: the power state of the simulated power supply.
        """
        if self._fail:
            raise ValueError("This power supply simulator is simulating failure.")
        return self._power_state


class PowerSupplyProxySimulator(
    MccsBaseComponentManager,
    PollingComponentManager[tuple[Optional[bool]], PowerState],
):
    """
    A component manager that simulates a proxy to an upstream power supply device.

    MCCS manages numerous hardware devices that can't turn turn
    themselves off and on directly. Rather, in order for a tango device
    to turn its hardware off or on, it needs to contact the tango device
    for an upstream device that supplies power to the hardware, and tell
    that tango device to supply/deny power to the hardware.

    However this functionality is largely not implemented yet, so we
    need to "fake" it. Plus, even when it _is_ implemented, there will
    be a need to simulate this when in simulation mode. This class
    provides that simulation pattern.
    """

    # pylint: disable=too-many-arguments
    def __init__(
        self: PowerSupplyProxySimulator,
        logger: logging.Logger,
        communication_state_callback: Optional[Callable],
        component_state_callback: Optional[Callable],
        initial_power_state: PowerState = PowerState.OFF,
        initial_fail: bool = False,
        _simulator: Optional[PowerSupplySimulator] = None,
    ) -> None:
        """
        Initialise a new instance.

        :param logger: a logger for this object to use
        :param communication_state_callback: callback to be called when
            the status of communications between the component manager
            and its component changes.
        :param component_state_callback: callback to be called when the
            state of the component changes.
        :param initial_power_state: the initial power state of the
            simulated power supply
        :param initial_fail: the initial failure condition of the
            simulated power supply. If True, the power supply will
            immediately simulate failure.
        """
        self._write_lock = threading.Lock()
        self._target_is_on: Optional[bool] = None
        self._pending_callback: Optional[Callable] = None
        self._active_callback: Optional[Callable] = None

        self._power_supply_simulator = _simulator or PowerSupplySimulator(
            initial_power_state=initial_power_state,
            initial_fail=initial_fail,
        )

        super().__init__(
            logger,
            communication_state_callback,  # type: ignore[arg-type]
            component_state_callback,  # type: ignore[arg-type]
        )

    def off(
        self: PowerSupplyProxySimulator, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Turn the power supply off.

        :param task_callback: callback to be called when the status of
            the command changes

        :return: task status and message
        """
        return self._set_power(False, task_callback)

    def standby(
        self: PowerSupplyProxySimulator, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Raise an error indicating that standby mode is not supported.

        :param task_callback: callback to be called when the status of
            the command changes

        :raises NotImplementedError: because this simulator does not
            support standby mode.
        """
        raise NotImplementedError("Standby mode is not supported.")

    def on(
        self: PowerSupplyProxySimulator, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Turn the power supply on.

        :param task_callback: callback to be called when the status of
            the command changes

        :return: task status and message
        """
        return self._set_power(True, task_callback)

    @check_communicating
    def _set_power(
        self: PowerSupplyProxySimulator,
        is_on: bool,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        with self._write_lock:
            if self._pending_callback is not None:
                self._pending_callback(
                    status=TaskStatus.ABORTED,
                    result=(ResultCode.ABORTED, "Superseded by later command."),
                )
            self._pending_callback = task_callback

            self._target_is_on = is_on

            if self._pending_callback is not None:
                self._pending_callback(status=TaskStatus.QUEUED)

        desired_state = "ON" if is_on else "OFF"

        return (
            TaskStatus.QUEUED,
            f"Power will be turned {desired_state} at next poll.",
        )

    def reset(
        self: PowerSupplyProxySimulator, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Reset the component.

        :param task_callback: callback to be called when the status of
            the command changes

        :raises NotImplementedError: because reset has not been
            implemented yet
        """
        raise NotImplementedError("PowerSupplyProxySimulator cannot be reset.")

    def abort_commands(
        self: PowerSupplyProxySimulator, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Abort all queued tasks.

        This implementation does not abort tasks that have already been
        picked up and executed by the poller.

        :param task_callback: callback to be called whenever the status
            of the task changes.

        :return: a task status and message.
        """
        if task_callback is not None:
            task_callback(status=TaskStatus.IN_PROGRESS)
        with self._write_lock:
            self._target_is_on = None
            if self._pending_callback is not None:
                self._pending_callback(
                    status=TaskStatus.ABORTED,
                    result=(ResultCode.ABORTED, "Aborted by explicit command."),
                )
                self._pending_callback = None
        if task_callback is not None:
            task_callback(status=TaskStatus.COMPLETED)
        return (TaskStatus.COMPLETED, "Pending tasks aborted.")

    def get_request(self: PowerSupplyProxySimulator) -> tuple[Optional[bool]]:
        """
        Return the reads and writes to be executed in the next poll.

        In this implementation, this simply returns an optional boolean
        that indicates whether or not to turn the power supply on or off.
        If true, we are asking to turn on the power supply;
        if false, we are asking to turn off the power supply;
        if None, we aren't asking for any action to be taken.

        :returns: reads and writes to be executed in the next poll.
        """
        with self._write_lock:
            if self._active_callback is None:
                self._active_callback = self._pending_callback
                self._pending_callback = None
            if self._active_callback is not None:
                self._active_callback(status=TaskStatus.IN_PROGRESS)

            target_is_on = self._target_is_on
            self._target_is_on = None
            # returning a unary tuple here, because target_is_on can be
            # None, but the poller is implemented not to poll at all if
            # get_request returns None.
            return (target_is_on,)

    def poll(
        self: PowerSupplyProxySimulator, poll_request: tuple[Optional[bool]]
    ) -> PowerState:
        """
        Poll the power supply.

        :param poll_request: specification of the reads and writes to be
            performed in this poll.
            In this implementation, this is simply an optional boolean
            that indicates whether or not to turn the power supply on or
            off.
            If True, this poll will turn on the power supply;
            if False, this poll will turn off the power supply;
            if None, no action will be taken.

        :return: responses to queries in this poll.
            In this implementation, this is simply a boolean that tells
            whether the power supply is on.
            If True, the power supply is currently on.
            If False, it is currently off.
        """  # noqa: DAR202
        (target_is_on,) = poll_request
        if target_is_on is True:
            self._power_supply_simulator.on()
        elif target_is_on is False:
            self._power_supply_simulator.off()
        return self._power_supply_simulator.power_state

    def poll_failed(self, exception: Exception) -> None:
        """
        Respond to an exception being raised by a poll attempt.

        This is a hook called by the poller when an exception occurs.

        :param exception: the exception that was raised by a recent poll
            attempt.
        """
        with self._write_lock:
            if self._active_callback is not None:
                self._active_callback(
                    status=TaskStatus.FAILED,
                    result=(ResultCode.FAILED, f"Poll failed: {str(exception)}"),
                )
                self._active_callback = None

        super().poll_failed(exception)

    def poll_succeeded(self, poll_response: PowerState) -> None:
        """
        Handle a successful poll, including any values received.

        This is a hook called by the poller at the end of each
        successful poll.

        :param poll_response: response to the poll, including any values
            received. In this implementation, this is simply a boolean
            indicating whether the power supply is on or not.
        """
        with self._write_lock:
            if self._active_callback is not None:
                # TODO: Can the poll_response return details on command results?
                self._active_callback(
                    status=TaskStatus.COMPLETED,
                    result=(ResultCode.OK, "Command completed"),
                )
                self._active_callback = None

        self._update_component_state(
            power=poll_response,
            fault=False,
        )
        super().poll_succeeded(poll_response)


# pylint: disable=abstract-method
class ComponentManagerWithUpstreamPowerSupply(TaskExecutorComponentManager):
    """
    A component manager for managing a component and a separate upstream power supply.

    MCCS manages numerous hardware devices that can't turn turn
    themselves off and on directly. Rather, in order for a Tango device
    to turn its hardware off or on, it needs to contact the Tango device
    for an upstream device that supplies power to the hardware, and tell
    that tango device to supply/deny power to the hardware.

    This class implements a pattern for this common situation.
    """

    # pylint: disable=too-many-arguments
    def __init__(
        self: ComponentManagerWithUpstreamPowerSupply,
        hardware_component_manager: MccsBaseComponentManager,
        power_supply_component_manager: MccsBaseComponentManager,
        logger: logging.Logger,
        communication_state_callback: Callable[[CommunicationStatus], None],
        component_state_callback: Optional[Callable[..., None]],
        **state: Any,
    ) -> None:
        """
        Initialise a new instance.

        :param hardware_component_manager: the component manager that
            manages the hardware (when it is turned on).
        :param power_supply_component_manager: the component
            manager that manages supply of power to the hardware.
        :param logger: a logger for this object to use
        :param communication_state_callback: callback to be called when
            the status of the communications channel between the
            component manager and its component changes
        :param component_state_callback: callback to be called when the
            component state changes
        :param state: keyword arguments specifying initial values for
            any other state to be reported by the component state
            changed callback
        """
        super().__init__(
            logger,
            communication_state_callback=communication_state_callback,
            component_state_callback=component_state_callback,
            power=None,
            fault=None,
            **state,
        )
        self._stopping_comms: bool = False
        self._power_supply_reported_power: Optional[PowerState] = None
        self._power_supply_communication_state = CommunicationStatus.DISABLED
        self._hardware_communication_state = CommunicationStatus.DISABLED

        self._power_supply_component_manager = power_supply_component_manager
        self._power_supply_component_manager.set_communication_state_callback(
            self._power_supply_communication_state_changed
        )
        self._power_supply_component_manager.set_component_state_callback(
            self._power_supply_component_state_changed
        )

        self._hardware_component_manager = hardware_component_manager
        self._hardware_component_manager.set_communication_state_callback(
            self._hardware_communication_state_changed
        )
        self._hardware_component_manager.set_component_state_callback(
            self._hardware_component_state_changed
        )

    def start_communicating(
        self: ComponentManagerWithUpstreamPowerSupply,
    ) -> None:
        """Establish communication with the hardware and the upstream power supply."""
        self._stopping_comms = False
        if (
            self._power_supply_component_manager.communication_state
            == CommunicationStatus.ESTABLISHED
        ):
            self._hardware_component_manager.start_communicating()
        else:
            self._power_supply_component_manager.start_communicating()

    def stop_communicating(
        self: ComponentManagerWithUpstreamPowerSupply,
    ) -> None:
        """Stop communication with the hardware and the upstream power supply."""
        # This flag will we used to stop communication with the power supply
        # once communication stopped with hardware.
        self._stopping_comms = True
        if self._hardware_communication_state != CommunicationStatus.DISABLED:
            self._hardware_component_manager.stop_communicating()
        else:
            self._power_supply_component_manager.stop_communicating()

    def _power_supply_communication_state_changed(
        self: ComponentManagerWithUpstreamPowerSupply,
        communication_state: CommunicationStatus,
    ) -> None:
        """
        Handle a change in status of communication.

        :param communication_state: the status of communication
        """
        self._power_supply_communication_state = communication_state
        self._evaluate_communication_state()

    def _hardware_communication_state_changed(
        self: ComponentManagerWithUpstreamPowerSupply,
        communication_state: CommunicationStatus,
    ) -> None:
        """
        Handle a change in status of communication with the hardware.

        :param communication_state: the status of communication with
            the hardware.
        """
        self._hardware_communication_state = communication_state
        self._evaluate_communication_state()

    def _evaluate_communication_state(
        self: ComponentManagerWithUpstreamPowerSupply,
    ) -> None:
        if self._power_supply_communication_state == CommunicationStatus.ESTABLISHED:
            if self._hardware_communication_state == CommunicationStatus.DISABLED:
                if self._stopping_comms:
                    self._stopping_comms = False
                    self._power_supply_component_manager.stop_communicating()
                return
            self._update_communication_state(self._hardware_communication_state)
        else:
            self._update_communication_state(self._power_supply_communication_state)

    def _power_supply_component_state_changed(
        self: ComponentManagerWithUpstreamPowerSupply,
        power: Optional[PowerState] = None,
        **kwargs: Any,
    ) -> None:
        """
        Handle a change in power state of the hardware.

        :param power: the power state of the hardware
        :param kwargs: any other keyword arguments
        """
        self._power_supply_reported_power = power
        if power is None:
            return
        if power == PowerState.ON:
            self._hardware_component_manager.start_communicating()
        elif power == PowerState.UNKNOWN:
            self._update_component_state(power=PowerState.UNKNOWN)
        else:  # NO_SUPPLY, OFF, STANDBY
            self._hardware_component_manager.stop_communicating()
            self._update_component_state(power=power)

    def _hardware_component_state_changed(
        self: ComponentManagerWithUpstreamPowerSupply,
        **kwargs: Any,
    ) -> None:
        power_state: PowerState | None = kwargs.get("power")
        if power_state is not None:
            if (
                power_state == PowerState.UNKNOWN
                and self._power_supply_reported_power
                in [
                    PowerState.OFF,
                    PowerState.NO_SUPPLY,
                    PowerState.STANDBY,
                ]
            ):
                self.logger.info(
                    "Power supply reports power "
                    f"{PowerState(self._power_supply_reported_power).name}. "
                    f"Hardware power reporting {PowerState(power_state).name}. "
                    "This is evaluated as "
                    f"{PowerState(self._power_supply_reported_power).name}. "
                )
                kwargs.update({"power": self._power_supply_reported_power})

        self._update_component_state(**kwargs)

    def off(
        self: ComponentManagerWithUpstreamPowerSupply,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Tell the upstream power supply proxy to turn the hardware off.

        :param task_callback: callback to be called when the status of
            the command changes

        :return: a task status and message.
        """
        return self._power_supply_component_manager.off(task_callback)

    def on(
        self: ComponentManagerWithUpstreamPowerSupply,
        task_callback: Optional[Callable] = None,
    ) -> tuple[TaskStatus, str]:
        """
        Tell the upstream power supply proxy to turn the hardware on.

        :param task_callback: callback to be called when the status of
            the command changes

        :return: a task status and message.
        """
        return self._power_supply_component_manager.on(task_callback)
