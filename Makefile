#
# Project makefile for a SKA low MCCS Common project. 
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE.txt for more info.

include .make/raw.mk
include .make/base.mk

PROJECT = ska-low-mccs-common


####################################
# DOCS
####################################
include .make/docs.mk

DOCS_SPHINXOPTS= -W --keep-going

docs-pre-build:
	poetry config virtualenvs.create false
	poetry install --no-root --only docs

.PHONY: docs-pre-build

####################################
# PYTHON
####################################
include .make/python.mk

PYTHON_LINE_LENGTH = 88
PYTHON_TEST_FILE = tests
PYTHON_VARS_AFTER_PYTEST = --forked

python-post-lint:
	$(PYTHON_RUNNER) mypy --config-file mypy.ini src/ tests

.PHONY: python-post-lint docs-pre-build


####################################
# OCI
####################################
include .make/oci.mk

OCI_IMAGE_BUILD_CONTEXT = $(PWD)


-include PrivateRules.mak


#######################################################################
# Helm
#
# This is a convenient place to temporarily upstream a k8s-test-runner
# helm chart for use in all MCCS repos. Ideally it would be upstreamed
# even further in future.
#######################################################################

HELM_CHARTS_TO_PUBLISH = ska-low-mccs-k8s-test-runner

include .make/helm.mk
