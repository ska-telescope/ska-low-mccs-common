# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module implements an abstract component manager for simple object components."""
from __future__ import annotations  # allow forward references in type hints

import logging
from typing import Callable, Optional

from ska_control_model import CommunicationStatus, TaskStatus
from ska_tango_base.base import check_communicating

from .component_manager import MccsBaseComponentManager
from .object_component import ObjectComponent

__all__ = ["ObjectComponentManager"]


# pylint: disable-next=abstract-method
class ObjectComponentManager(MccsBaseComponentManager):
    """
    An abstract component manager for a component that is an object in this process.

    The base component manager is a very general class that allows for
    management of remote components to which communication may need to
    be established and maintained. In cases where the component is
    simply an object running in the same process as its component
    manager (for example, a simple simulator), such complexity is not
    needed. This class eliminates that complexity, providing a basic
    framework for these simple component managers.
    """

    def __init__(
        self: ObjectComponentManager,
        component: ObjectComponent,
        logger: logging.Logger,
        communication_state_callback: Callable[[CommunicationStatus], None],
        component_state_callback: Callable[..., None],
    ) -> None:
        """
        Initialise a new instance.

        :param component: the component managed by this component
            manager
        :param logger: a logger for this object to use
        :param communication_state_callback: callback to be
            called when the status of the communications channel between
            the component manager and its component changes
        :param component_state_callback: callback to be called
            when the component state changes.
        """
        self._component = component
        self._fail_communicate = False

        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=None,
            fault=None,
        )

    def start_communicating(self: ObjectComponentManager) -> None:
        """
        Establish communication with the component, then start monitoring.

        :raises ConnectionError: if the attempt to establish
            communication with the channel fails.
        """
        if self.communication_state == CommunicationStatus.ESTABLISHED:
            return
        self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)

        if self._fail_communicate:
            raise ConnectionError("Failed to connect")

        self._update_communication_state(CommunicationStatus.ESTABLISHED)

        self._component.set_power_mode_changed_callback(self._update_component_state)
        self._component.set_fault_callback(self._update_component_state)

    def stop_communicating(self: ObjectComponentManager) -> None:
        """Cease monitoring the component, and break off all communication with it."""
        if self.communication_state == CommunicationStatus.DISABLED:
            return

        self._component.set_fault_callback(None)
        self._component.set_power_mode_changed_callback(None)

        self._update_communication_state(CommunicationStatus.DISABLED)
        self._update_component_state(power=None, fault=None)

    def simulate_communication_failure(
        self: ObjectComponentManager, fail_communicate: bool
    ) -> None:
        """
        Simulate (or stop simulating) a failure to communicate with the component.

        :param fail_communicate: whether the connection to the component
            is failing
        """
        self._fail_communicate = fail_communicate
        if (
            fail_communicate
            and self.communication_state == CommunicationStatus.ESTABLISHED
        ):
            self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)

    @check_communicating
    def off(
        self: ObjectComponentManager, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Turn the component off.

        :param task_callback: Update task state, defaults to None

        :return: a taskstatus and message.
        """
        return self._component.off(task_callback)

    @check_communicating
    def standby(
        self: ObjectComponentManager, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Put the component into low-power standby mode.

        :param task_callback: Update task state, defaults to None

        :return: a taskstatus and message
        """
        return self._component.standby(task_callback)

    @check_communicating
    def on(
        self: ObjectComponentManager, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Turn the component on.

        :param task_callback: Update task state, defaults to None

        :return: a taskstatus and message
        """
        return self._component.on(task_callback)

    @check_communicating
    def reset(
        self: ObjectComponentManager, task_callback: Optional[Callable] = None
    ) -> tuple[TaskStatus, str]:
        """
        Reset the component.

        :param task_callback: Update task state, defaults to None

        :return: a taskstatus and message
        """
        return self._component.reset(task_callback)
