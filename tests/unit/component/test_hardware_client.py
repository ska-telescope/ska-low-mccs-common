# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""This module contains tests of the hardware client module."""
from __future__ import annotations

from typing import Any

import pytest
import requests

from ska_low_mccs_common.component import (
    HardwareClientResponseStatusCodes,
    WebHardwareClient,
)


# pylint: disable=too-few-public-methods
class TestWebHardwareClient:
    """Tests of the WebHardwareClient class."""

    def test_web_hardware_client_interface(
        self: TestWebHardwareClient,
        monkeypatch: pytest.MonkeyPatch,
    ) -> None:
        """
        Test the web hardware client.

        This test monkeypatches the requests library to return canned
        mock responses. It therefore only tests that the
        ``WebHardwareClient`` class drivers the requests library
        correctly.

        :param monkeypatch: the pytest monkey-patching fixture
        """
        good_ip = "192.0.2.0"
        a_bad_ip = "192.0.2.1"

        # pylint: disable=too-few-public-methods
        class MockResponse:
            """A mock class to replace requests.Response."""

            status_code = requests.codes["ok"]

            @staticmethod
            def json() -> dict[str, str]:
                """
                Replace the patched request.Response.json with mock.

                This implementation always returns the same key-value pair.

                :return: a dictionary with a single key-value pair in it.
                """
                return {"mock_key": "mock_value"}

        def mock_get(url: str, params: Any = None, **kwargs: Any) -> MockResponse:
            """
            Replace requests.get with mock method.

            :param url: the URL
            :param params: arguments to the GET
            :param kwargs: other keyword args

            :raises RequestException: if the URL
                provided is not to the one and only good IP address
                supported by this patch mock.

            :return: a response
            """
            if good_ip in url:
                return MockResponse()
            raise requests.exceptions.RequestException(
                f"Cannot connect to URL {url}: Test harness has monkeypatched "
                "requests.get() so that successful connections can only be made to "
                f"{good_ip}."
            )

        monkeypatch.setattr(requests, "get", mock_get)

        client = WebHardwareClient(a_bad_ip, 80)
        info_message = (
            f"Exception: Cannot connect to URL http://{a_bad_ip}:80/get/json.htm: "
            "Test harness has monkeypatched requests.get() so that successful "
            f"connections can only be made to {good_ip}."
        )
        assert client.execute_command("Foo") == {
            "status": HardwareClientResponseStatusCodes.REQUEST_EXCEPTION.name,
            "info": info_message,
            "command": "Foo",
            "retvalue": "",
        }
        assert client.get_attribute("foo") == {
            "status": HardwareClientResponseStatusCodes.REQUEST_EXCEPTION.name,
            "info": info_message,
            "attribute": "foo",
            "value": None,
        }
        assert client.set_attribute("foo", "bah") == {
            "status": HardwareClientResponseStatusCodes.REQUEST_EXCEPTION.name,
            "info": info_message,
            "attribute": "foo",
            "value": None,
        }

        client = WebHardwareClient(good_ip, 80)

        assert client.execute_command("Foo") == {
            "mock_key": "mock_value",
        }
        assert client.get_attribute("foo") == {
            "mock_key": "mock_value",
        }
        assert client.set_attribute("foo", "bah") == {
            "mock_key": "mock_value",
        }
